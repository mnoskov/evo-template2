<?php

    return [
        'title' => 'Сотрудники',

        'show_in_templates' => $this->modx->templateConstants['employees_template_id'],

        'placement' => 'content',

        'templates' => [
            'owner' => '
                <div class="employees">
                    [+wrap+]
                </div>
            ',

            'cycle' => [
                'owner' => '
                    <div class="employees-cycle">
                        <div class="slick" data-slick=\'{"arrows": true, "autoplay": true, "slidesToShow": 4, "responsive": [{"breakpoint": 1200, "settings": {"slidesToShow": 3}}, {"breakpoint": 767, "settings": {"slidesToShow": 2}}, {"breakpoint": 450, "settings": {"slidesToShow": 1}}]}\'>
                            [+wrap+]
                        </div>
                    </div>
                ',
            ],
        ],
    ];

