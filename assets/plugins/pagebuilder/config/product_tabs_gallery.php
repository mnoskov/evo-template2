<?php

return [
    'title' => 'Галерея',

    'container' => 'product_tabs',

    'templates' => [
        'owner' => '
            <div id="usertab[+iteration+]">
                <div class="images-cycle">
                    <div class="images slick" id="gallery[+iteration+]" data-slick=\'{"asNavFor": "#thumbs[+iteration+]"}\'>
                        [+images+]
                    </div>

                    <div class="thumbs slick" id="thumbs[+iteration+]" data-slick=\'{"slidesToShow": 5, "swipeToSlide": true, "asNavFor": "#gallery[+iteration+]", "focusOnSelect": true, "arrows": false, "responsive": [{"breakpoint": 576, "settings": {"slidesToShow": 3}}]}\'>
                        [+thumbs/images+]
                    </div>
                </div>
            </div>
        ',

        'images' => '
            <div class="slide">
                <a href="[+image+]" data-fancybox="images[+iteration+]" title="[+text+]" style="background-image: url([[thumb? &input=`[+image+]` &options=`w=800,h=320`]]);"></a>
                <div class="slide-title">[+text+]</div>
            </div>
        ',

        'head' => [
            'owner' => '<a href="#usertab[+iteration+]">[+title+]</a>',
        ],

        'thumbs' => [
            'images' => '
                <div class="slide">
                    <div style="background-image: url([[thumb? &input=`[+image+]` &options=`w=161,h=101,f=jpg,far=C,bg=FFFFFF`]]);"></div>
                </div>
            ',
        ],
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок',
            'type'    => 'text',
        ],

        'images' => [
            'caption' => 'Изображения',
            'type'    => 'group',
            'layout'  => 'gallery',
            'fields'  => [
                'image' => [
                    'caption' => 'Изображение',
                    'type' => 'image',
                ],

                'text' => [
                    'caption' => 'Подпись',
                    'type' => 'textarea',
                    'height' => '80px',
                ],
            ],
        ],
    ],
];

