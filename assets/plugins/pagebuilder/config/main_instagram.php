<?php

return [
    'title' => 'Лента Instagram',

    'show_in_templates' => 1,

    'container' => 'main',

    'templates' => [
        'owner' => '
            <div class="section">
                <div class="container">
                    <div class="to-all">
                        <a href="https://www.instagram.com/[+login+]/" target="_blank" rel="nofollow">
                            Все публикации
                        </a>
                    </div>

                    <div class="block-title">
                        [+title+]
                    </div>

                    [!DLInstagram? 
                        &token=`[+token+]`
                        &display=`7`
                        &tpl=`main_instagram_item`
                        &ownerTPL=`main_instagram_wrap`
                        &dateFormat=`%e %B`
                    !]
                </div>
            </div>
        ',
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок блока',
            'type' => 'text',
            'default' => 'Мы в Instagram',
        ],

        'login' => [
            'caption' => 'Логин в Instagram',
            'type' => 'text',
        ],

        'token' => [
            'caption' => 'Токен',
            'type' => 'textarea',
            'height' => 100,
            'note' => 'Инструкция по получению токена - <a href="https://github.com/mnoskov/DLInstagram/blob/master/INSTRUCTION.md" target="_blank">https://github.com/mnoskov/DLInstagram/blob/master/INSTRUCTION.md</a>',
        ],
    ],
];

