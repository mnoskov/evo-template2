<?php

return [
    'title' => 'Отзывы',

    'show_in_templates' => 1,

    'container' => 'main',

    'templates' => [
        'owner' => '
            <div class="section">
                <div class="container">
                    <div class="to-all">
                        <a href="[~53~]">[+btn_label+]</a>
                    </div>

                    <div class="block-title">
                        [+title+]
                    </div>

                    [[DocLister? 
                        &parents=`53`
                        &tvList=`image`
                        &tpl=`reviews_slide`
                        &ownerTPL=`reviews_cycle`
                        &display=`6`
                    ]]

                    <div class="text-xs-center">
                        <a href="#" class="btn btn-theme" data-toggle="modal" data-target="#review">Оставить отзыв</a>
                    </div>
                </div>

                {{modal_review}}
            </div>
        ',
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок блока',
            'type' => 'text',
            'default' => 'Отзывы',
        ],

        'btn_label' => [
            'caption' => 'Надпись на кнопке',
            'type' => 'text',
            'default' => 'Все отзывы',
        ],
    ],
];
