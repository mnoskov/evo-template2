<?php

return [
    'title' => 'Партнеры',

    'show_in_templates' => 1,

    'container' => 'main',

    'templates' => [
        'owner' => '
            <div class="partners section">
                <div class="container">
                    <div class="block-title">
                        [+title+]
                    </div>

                    <div class="slick" data-slick=\'{"arrows": true, "autoplay": true, "slidesToShow": 5, "responsive": [{"breakpoint": 991, "settings": {"slidesToShow": 4}}, {"breakpoint": 767, "settings": {"slidesToShow": 3}}, {"breakpoint": 575, "settings": {"slidesToShow": 2}}]}\'>
                        [+items+]
                    </div>
                </div>
            </div>
        ',

        'items' => '
            <span class="slide">
                <img class="lazy" data-src="[[phpthumb? &input=`[+image+]` &options=`w=150,h=100` &adBlockFix=`1`]]" alt="[+title+]">
            </span>
        ',
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок блока',
            'type'    => 'text',
            'default' => 'Наши партнеры',
        ],
        
        'items' => [
            'caption' => 'Логотипы',
            'type'    => 'group',
            'layout'  => 'gallery',
            'fields'  => [
                'title' => [
                    'caption' => 'Название',
                    'type'    => 'text',
                ],

                'image' => [
                    'caption' => 'Логотип',
                    'type'    => 'image',
                ],
            ],
        ],
    ],
];

