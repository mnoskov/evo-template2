<?php

return [
    'title' => 'Сотрудники',

    'container' => 'main',

    'templates' => [
        'owner' => '
            <div class="section">
                <div class="container">
                    <div class="block-title">
                        [+title+]
                    </div>

                    [[PageBuilder? &docid=`[+parent+]` &container=`employees` &templates=`cycle`]]
                </div>
            </div>
        ',
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок блока',
            'type' => 'text',
            'default' => 'Сотрудники',
        ],

        'parent' => [
            'caption' => 'Источник',
            'type' => 'dropdown',
            'elements' => '@SELECT pagetitle, id FROM ' . $this->modx->getFullTablename('site_content') . ' WHERE template = \'' . $this->modx->templateConstants['employees_template_id'] . '\' AND deleted = 0 ORDER BY menuindex',
            'default' => '52',
        ],
    ],
];

