<?php

    return [
        'title' => 'Хиты продаж',

        'show_in_templates' => 1,

        'container' => 'main',

        'templates' => [
            'owner' => '
                <div class="section">
                    <div class="container">
                        <div class="to-all">
                            <a href="[~[+source+]~]">[+btn+]</a>
                        </div>

                        <div class="block-title">
                            [+title+]
                        </div>

                        [[DocLister? 
                            &parents=`[+source+]`
                            &depth=`4` 
                            &tvList=`image,price,old_price`
                            &orderBy=`price ASC`
                            &tpl=`products_item` 
                            &ownerTPL=`products_cycle`
                            &addWhereList=`c.template = ' . $this->modx->db->getValue($this->modx->db->select('id', $this->modx->getFullTableName('site_templates'), "templatename = 'product'")) . '`
                            &filters=`tv:best:=:1`
                        ]]
                    </div>
                </div>
            ',
        ],

        'fields' => [
            'title' => [
                'caption' => 'Заголовок',
                'type'    => 'text',
            ],

            'btn' => [
                'caption' => 'Текст кнопки',
                'type'    => 'text',
            ],

            'source' => [
                'caption' => 'Источник данных',
                'type' => 'dropdown',
                'elements' => '@SELECT pagetitle, id FROM ' . $this->modx->getFullTablename('site_content') . ' WHERE parent = 1 AND deleted = 0',
                'default' => 11,
            ],
        ],
    ];

