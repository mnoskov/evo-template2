<?php

return [
    'title' => 'Слайд упрощенный',

    'show_in_templates' => 1,

    'container' => 'main_cycle',

    'templates' => [
        'owner' => '
            <div class="slide slide-simple" data-invert="[+invert+]"[[if? &is=`[+color+]:!empty` &then=` style="color: [+color+]"`]]>
                <div class="slide-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h1 class="title">
                                    [[if? &is=`[+text_decoration+]:is:bg` &then=`
                                        <span class="inline-padding"><span><span>[+title+]</span></span></span>
                                    ` &else=`
                                        <span class="[+text_decoration+]">[+title+]</span>
                                    `]]
                                </h1>

                                <div class="text user-content">
                                    [+text+]
                                </div>

                                [[if? &is=`[+link+]~~!empty` &separator=`~~` &then=`
                                    <a href="[+link+]" class="btn btn-theme">Подробнее</a>
                                `]]
                            </div>
                        </div>
                    </div>

                    <div class="image lazy" style="background-color: [[getImageColor? &source=`[+background+]`]]" data-src="[+background+]"></div>
                </div>
            </div>
        ',
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок',
            'type'    => 'text',
        ],

        'color' => [
            'caption' => 'Цвет текста',
            'type'    => 'color',
        ],

        'background' => [
            'caption' => 'Фоновое изображение',
            'type'    => 'image',
        ],

        'text' => [
            'caption' => 'Текст',
            'type'    => 'richtext',
            'options' => [
                'height' => '100px',
            ],
        ],

        'text_decoration' => [
            'caption'  => 'Оформление текста',
            'type'     => 'radio',
            'elements' => ['' => 'Нет', 'bg' => 'Подложка', 'glowing' => 'Свечение', 'shadow' => 'Тень'],
            'layout'   => 'horizontal',
        ],

        'link' => [
            'caption' => 'Ссылка',
            'type'    => 'text',
        ],

        'invert' => [
            'caption' => 'Инвертировать',
            'type'    => 'checkbox',
            'elements' => [1 => 'Да'],
        ],
    ],
];
