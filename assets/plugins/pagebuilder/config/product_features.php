<?php

return [
    'title' => 'Преимущество',

    'container' => 'product_features',

    'templates' => [
        'owner' => '
            <li>
                <span class="image">
                    <img src="[[phpthumb? &input=`[+image+]` &options=`w=100,h=100,zc=1`]]">
                </span><span class="feature-card">
                    <div class="title">[+title+]</div>
                    [+text+]
                </span>
        ',
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок',
            'type'    => 'text',
            'layout'  => 'col-6',
        ],

        'image' => [
            'caption' => 'Иконка',
            'type'    => 'image',
            'layout'  => 'col-6',
        ],

        'text' => [
            'caption' => 'Текст',
            'type'    => 'textarea',
            'height'  => '60px',
        ],
    ],
];
