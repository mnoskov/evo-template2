<?php

return [
    'title' => 'Характеристики',

    'container' => 'product_tabs',

    'templates' => [
        'owner' => '
            <div id="usertab[+iteration+]">
                [[if? &is=`[+content+]~~!empty` &separator=`~~` &then=`
                    <div class="user-content">
                        [+content+]
                    </div>
                `]]

                [[renderAttributes?
                    &category=`' . $this->modx->templateConstants['attributes_category_id'] . '`
                    &tpl=`product_attributes_row`
                    &outerTpl=`product_attributes_wrap`
                    &exclude=`category,price,certification_number,vin`
                ]]
            </div>
        ',

        'head' => [
            'owner' => '<a href="#usertab[+iteration+]">[+title+]</a>',
        ],
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок',
            'type'    => 'text',
            'default' => 'Характеристики',
        ],

        'content' => [
            'caption' => 'Содержимое',
            'type'    => 'richtext',
            'options' => [
                'height' => '200px',
            ],
        ],
    ],
];
