<?php

return [
    'title' => 'Статьи',

    'show_in_templates' => 1,

    'container' => 'main',

    'templates' => [
        'owner' => '
            <div class="section">
                <div class="container">
                    <div class="to-all">
                        <a href="[~[+source+]~]">[+btn_label+]</a>
                    </div>

                    <div class="block-title">
                        [+title+]
                    </div>

                    [[DocLister? 
                        &parents=`[+source+]`
                        &tvList=`image`
                        &tpl=`main_articles_item`
                        &ownerTPL=`main_articles_wrap`
                        &orderBy=`c.pub_date DESC`
                        &depth=`4`
                        &addWhereList=`c.template = \'[+template+]\'`
                        &prepare=`prepareMainArticlesItem`
                        &display=`5`
                    ]]
                </div>
            </div>
        ',
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок блока',
            'type' => 'text',
            'default' => 'Категории',
        ],

        'btn_label' => [
            'caption' => 'Надпись на кнопке',
            'type' => 'text',
            'default' => 'Все категории',
        ],

        'source' => [
            'caption' => 'Источник данных',
            'type' => 'dropdown',
            'elements' => '@SELECT pagetitle, id FROM ' . $this->modx->getFullTablename('site_content') . ' WHERE parent = 1 AND deleted = 0 ORDER BY menuindex',
            'default' => 11,
        ],

        'template' => [
            'caption' => 'Шаблон ресурсов',
            'type' => 'dropdown',
            'elements' => '@SELECT templatename, id FROM ' . $this->modx->getFullTablename('site_templates') . ' ORDER BY templatename',
            'default' => 2,
        ],
    ],
];

