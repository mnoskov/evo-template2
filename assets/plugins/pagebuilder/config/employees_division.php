<?php

return [
    'title' => 'Сотрудники',

    'container' => 'employees',

    'templates' => [
        'owner' => '
            <div class="employees-division">
                <h3>[+title+]</h3>

                <ul class="items">
                    [+employees+]
                </ul>
            </div>
        ',

        'employees' => '
            <li>
                <div class="employee-item">
                    <img src="[[phpthumb? &input=`[+image+]` &options=`w=190,h=190,zc=1` &adBlockFix=`1`]]" alt="[+name_e+]" class="img-fluid">
                    <div class="name">[+name+]</div>
                    <div class="text-muted small">[+role+]</div>
                    <div class="description">[+description+]</div>

                    [[if? &is=`[+email+]:!empty` &then=`
                        <a href="mailto:[+email+]">[+email+]</a><br>
                    `]]

                    [[if? &is=`[+phone+]:!empty` &then=`
                        [+phone+]<br>
                    `]]
                    
                    [[if? &is=`[+content+]~~!empty` &separator=`~~` &then=`
                        <div class="user-content hidden">
                            [+content+]
                        </div>
                            
                        <a href="#" data-toggle="modal" data-target="#employee">Подробнее</a>
                    `]]
                </div>
        ',

        'cycle' => [
            'owner' => '[+employees+]',

            'employees' => '
                <div class="slide">
                    <div class="employee-item">
                        <img src="[[phpthumb? &input=`[+image+]` &options=`w=190,h=190,zc=1` &adBlockFix=`1`]]" alt="[+name_e+]" class="img-fluid">
                        <div class="name">[+name+]</div>
                        <div class="text-muted small">[+role+]</div>
                        <div class="description">[+description+]</div>

                        [[if? &is=`[+email+]:!empty` &then=`
                            <a href="mailto:[+email+]">[+email+]</a><br>
                        `]]

                        [[if? &is=`[+phone+]:!empty` &then=`
                            [+phone+]<br>
                        `]]
                        
                        [[if? &is=`[+content+]~~!empty` &separator=`~~` &then=`
                            <div class="user-content hidden">
                                [+content+]
                            </div>
                                
                            <a href="#" data-toggle="modal" data-target="#employee">Подробнее</a>
                        `]]
                    </div>
                </div>
            ',
        ],
    ],

    'fields' => [
        'title' => [
            'caption' => 'Название отдела',
            'type'    => 'text',
        ],

        'employees' => [
            'caption' => 'Сотрудники',
            'type'    => 'group',
            'fields'  => [
                'name' => [
                    'caption' => 'Имя',
                    'type'    => 'text',
                ],

                'role' => [
                    'caption' => 'Должность',
                    'type'    => 'text',
                ],

                'image' => [
                    'caption' => 'Изображение',
                    'type'    => 'image',
                ],

                'description' => [
                    'caption' => 'Краткая характеристика',
                    'type'    => 'textarea',
                ],

                'content' => [
                    'caption' => 'Полная характеристика',
                    'type'    => 'richtext',
                    'theme'   => 'mini',
                ],

                'phone' => [
                    'caption' => 'Телефон',
                    'type'    => 'text',
                ],

                'email' => [
                    'caption' => 'Email',
                    'type'    => 'text',
                ],

            ],
        ],
    ],
];

