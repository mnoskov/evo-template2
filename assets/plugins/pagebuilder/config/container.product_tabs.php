<?php

return [
    'title' => 'Табы',

    'show_in_templates' => $this->modx->templateConstants['product_template_id'],

    'placement' => 'content',

    'templates' => [
        'owner' => '
            [+wrap+]
        ',
    ],

    'defaults' => [
        [
            'block' => 'product_tabs_attrs',
            'values' => [
                'title'   => 'Характеристики',
                'content' => '',
            ],
        ]
    ],
];
