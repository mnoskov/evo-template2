<?php

return [
    'title' => 'Контент',

    'container' => 'product_tabs',

    'templates' => [
        'owner' => '
            <div id="usertab[+iteration+]">
                <div class="user-content">
                    [+content+]
                </div>
            </div>
        ',

        'head' => [
            'owner' => '<a href="#usertab[+iteration+]">[+title+]</a>',
        ],
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок',
            'type'    => 'text',
            'default' => 'Описание',
        ],

        'content' => [
            'caption' => 'Содержимое',
            'type'    => 'richtext',
        ],
    ],
];
