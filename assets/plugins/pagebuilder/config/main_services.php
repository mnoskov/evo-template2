<?php

return [
    'title' => 'Услуги',

    'show_in_templates' => 1,

    'container' => 'main',

    'templates' => [
        'owner' => '
            <div class="section">
                <div class="container">
                    <div class="to-all">
                        <a href="[~[+source+]~]">[+btn_label+]</a>
                    </div>

                    <div class="block-title">
                        [+title+]
                    </div>

                    <div class="row">
                        <div class="col-xl-3">
                            <div class="user-content">
                                [+text+]
                            </div>

                            <a href="[~[+source+]~]" class="btn btn-theme">
                                Подробнее
                            </a>
                        </div>

                        <div class="col-xl-9">
                            [[DocLister? 
                                &parents=`[+source+]`
                                &showParent=`-1`
                                &tvList=`image`
                                &tpl=`main_services_item`
                                &ownerTPL=`main_services_wrap`
                                &orderBy=`menuindex ASC`
                                &depth=`4`
                                &addWhereList=`c.template = \'[+template+]\'`
                                &filters=`tv:show_on_main:=:1`
                                &prepare=`prepareMainServicesItem`
                                &display=`6`
                            ]]
                        </div>
                    </div>
                </div>
            </div>
        ',
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок блока',
            'type' => 'text',
            'default' => 'Категории',
        ],

        'btn_label' => [
            'caption' => 'Надпись на кнопке',
            'type' => 'text',
            'default' => 'Все категории',
        ],

        'source' => [
            'caption' => 'Источник данных',
            'type' => 'dropdown',
            'elements' => '@SELECT pagetitle, id FROM ' . $this->modx->getFullTablename('site_content') . ' WHERE parent = 1 AND deleted = 0 ORDER BY menuindex',
            'default' => 11,
        ],

        'template' => [
            'caption' => 'Шаблон ресурсов',
            'type' => 'dropdown',
            'elements' => '@SELECT templatename, id FROM ' . $this->modx->getFullTablename('site_templates') . ' ORDER BY templatename',
            'default' => 2,
        ],

        'text' => [
            'caption' => 'Текст',
            'type' => 'richtext',
            'options' => [
                'height' => 150,
            ],
        ],
    ],
];

