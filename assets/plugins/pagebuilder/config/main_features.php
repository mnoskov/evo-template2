<?php

    return [
        'title' => 'Преимущества компании',

        'show_in_templates' => 1,

        'container' => 'main',

        'templates' => [
            'owner' => '
                <div class="company-features section gray">
                    <div class="container">
                        <div class="row">
                            [+items+]
                        </div>
                    </div>
                </div>
            ',

            'items' => '
                <div class="col-md-4">
                    <img src="[[phpthumb? &input=`[+image+]` &options=`w=100,h=100,zc=1` &adBlockFix=`1`]]">
                    <div class="feature-card">
                        <div class="title">[+title+]</div>
                        [+text+]
                    </div>
                </div>
            ',
        ],

        'fields' => [
            'items' => [
                'caption' => 'Преимущества',
                'type'    => 'group',
                'fields'  => [
                    'title' => [
                        'caption' => 'Заголовок',
                        'type'    => 'text',
                    ],

                    'image' => [
                        'caption' => 'Изображение',
                        'type'    => 'image',
                    ],

                    'text' => [
                        'caption' => 'Текст',
                        'type'    => 'textarea',
                        'height'  => '60px',
                    ],
                ],
            ],
        ],
    ];

