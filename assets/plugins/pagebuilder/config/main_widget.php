<?php

return [
    'title' => 'Виджет',

    'show_in_templates' => 1,

    'container' => 'main',

    'templates' => [
        'owner' => '
            <div class="section">
                <div class="container">
                    [[if? &is=`[+title+]~~!empty` &separator=`~~` &then=`
                        <div class="block-title">
                            [+title+]
                        </div>
                    `]]

                    [+widget_code+]
                </div>
            </div>
        ',
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок блока',
            'type' => 'text',
            'default' => 'Сторонний виджет',
        ],

        'widget_code' => [
            'caption' => 'Код виджета',
            'type' => 'textarea',
            'height' => '150px',
        ],
    ],
];

