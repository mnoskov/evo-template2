<?php

    return [
        'title' => 'Слайдер',

        'show_in_templates' => 1,

        'placement' => 'tab',

        'templates' => [
            'owner' => '
                <div class="main-cycle">
                    <div class="slick" data-slick=\'{"slide": ".slide", "dots": true, "arrows": true, "autoplay": true, "autoplaySpeed": 5000, "appendArrows": ".main-cycle .arrows .container"}\'>
                        [+wrap+]
                        <div class="arrows">
                            <div class="container"></div>
                        </div>
                    </div>
                </div>
            ',
        ],
    ];

