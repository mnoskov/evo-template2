<?php

return [
    'title' => 'Боли',

    'container' => 'main',

    'templates' => [
        'owner' => '
            <div class="section section-pain">
                <div class="container">
                    <div class="block-title">
                        [+title+]
                    </div>

                    <ul class="items">
                        [+items+]
                    </ul>
                </div>
            </div>
        ',

        'items' => '
            <li>
                <div class="item">
                    <div class="image">
                        <div style="background-image: url(\'[[phpthumb? &input=`[+image+]` &options=`w=322,h=215,zc=1` &adBlockFix=`1`]]\'); background-color: [[getImageColor? &source=`[+image+]`]];"></div>
                    </div>

                    <div class="text">
                        [+text+]
                    </div>
                </div>
        ',
    ],

    'fields' => [
        'title' => [
            'caption' => 'Заголовок блока',
            'type' => 'text',
            'default' => 'Немножко боли',
        ],

        'items' => [
            'caption' => 'Боли',
            'type'    => 'group',
            'fields'  => [
                'image' => [
                    'caption' => 'Изображение',
                    'type'    => 'image',
                ],

                'text' => [
                    'caption' => 'Текст',
                    'type'    => 'textarea',
                    'height'  => '60px',
                ],
            ],
        ],
    ],
];
