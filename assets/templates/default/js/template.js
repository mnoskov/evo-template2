var $headPlaceholder = $('.head-placeholder'),
    $mainCycle = $('.main-cycle');

$('.main-cycle .slick').on('beforeChange init', function(e, slick, current, next) {
    if (typeof next == 'undefined') {
        next = slick.currentSlide;
    }

    var $slide = slick.$slides.eq(next),
        invert = $slide.attr('data-invert') == 1;

    $headPlaceholder.toggleClass('inverted', invert && !$slide.hasClass('slide-simple'));
    $mainCycle.toggleClass('inverted', invert);
});

$('.slick').on('beforeChange init', function(e, slick, current, next) {
    if (!slick.$slides) {
        return;
    }

    var $slides = slick.$slides.not('.lazy-loaded').filter('.slick-active, .slick-current');

    if (typeof current != 'undefined') {
        $slides = $slides.add(slick.$slides.eq(current));
    }

    if (typeof next != 'undefined') {
        $slides = $slides.add(slick.$slides.eq(next));
    }

    if ($slides.length) {
        var $allSlides = slick.$slideTrack.children('.slick-slide'),
            rangeStart = $allSlides.index($slides.first()),
            rangeEnd   = $allSlides.index($slides.last());

        $slides = $slides.add($allSlides.slice(Math.max(0, rangeStart - slick.options.slidesToShow), rangeStart));
        $slides = $slides.add($allSlides.slice(rangeEnd + 1, rangeEnd + slick.options.slidesToShow + 1));
    }

    $slides = $slides.not('.lazy-loaded');

    $slides.find('[data-src]').add($slides.filter('[data-src]')).each(function() {
        var $self = $(this),
            image = $self.attr('data-src');

        if (this.tagName == 'IMG') {
            $self.attr('src', image);
        } else {
            $self.css('background-image', 'url(' + image + ')');
        }

        $self.removeAttr('data-src');

        if (!$self.hasClass('slick-slide')) {
            $self = $self.closest('.slick-slide');
        }

        $self.addClass('lazy-loaded');
    });
});

$(function() {
    $('.lazy').each(function() {
        if ($(this).parents().filter('.slick').length) {
            return;
        }

        $(this).lazy();
    });

    $(document).on('click', '#main-submit', function(e) {
        this.form.action = this.getAttribute('data-url');
        $(document).off('submit', 'form#eFiltr');
    });

    $(document).on('submit', 'form#eFiltr', function() {
        $(this).find('.fltr_inner').each(function() {
            var $values = $(this).children('.value').children('input'),
                $limits = $(this).children('.value-limits').children('span');

            $values.each(function(i) {
                if (this.value == $limits.get(i).innerText) {
                    this.disabled = true;
                }
            });
        });
    });

    $('.user-content > table').wrap('<div class="table-responsive"/>');

    $('[data-gallery="fancybox"]').fancybox();

    if ($.fn.flexTabs) {
        $('.tabs-wrapper > [id]')
            .on('afterOpen.ft', function(e, instance, target) {
                var $slick = target.content.find('.slick');

                if ($slick.length) {
                    if ($slick.hasClass('slick-initialized')) {
                        $slick.slick('unslick');
                    }

                    setTimeout(function() {
                        $slick.slick(slickDefaults);
                    }, 100);
                }
            })
            .flexTabs({
                collapsible: false,
                breakpoint: 975,
                fade: 0
            });
    }

    (function($head) {
        var isMain = $(document.body).hasClass('main-page');

        $head.addClass('loaded');

        $(window)
            .on('scroll resize', function() {
                var fixed = $head.hasClass('fixed');

                if ($(this).scrollTop() > 250) {
                    if (!fixed) {
                        $head.addClass('fixed').removeClass('static');
                    }
                } else {
                    if (fixed) {
                        $head.removeClass('fixed').addClass('static');
                        $head.children('.head-menu').removeClass('show-fixed').find('.menu').removeAttr('style');
                    }
                }

                if (!fixed) {
                    var height = $head.children('.head').outerHeight() + (window.innerWidth > 767 ? $head.children('.head-menu').outerHeight() : 0);

                    if (isMain) {
                        $head.nextAll('.main-cycle').find('.slide').css('padding-top', height);
                    } else {
                        $head.css('height', height);
                    }
                }
            })
            .load(function() {
                $(this).scroll();
            })
            .scroll();
    })($('.head-placeholder'));

    $('.instagram-list .hidden').scrollbar();

    $('.props').runOnScroll(function($block) {
        var $items = $block.find('.value.animate'),
            max = 1;

        $items.each(function() {
            max = Math.max(max, $(this).attr('data-value'));
        });

        max = 1 / max;

        $items.each(function() {
            var val = $(this).attr('data-value');
            $(this).animateNumber({number: val}, 2000 + val * max * 1000);
        });
    });

    $('.toggle-menu').click(function() {
        $('.category-menu-item > .submenu').removeAttr('style');
    });

    $('.menu-overlay').click(function() {
        $(document.body).toggleClass('menu-opened');
        $('.category-menu-item > .submenu').removeAttr('style');
    });

    $('.toggle-search').click(function() {
        $(document.body).toggleClass('search-opened');
    });

    $('.toggle-fixed-filters, .fixed-filters-overlay').click(function() {
        $(document.body).toggleClass('fixed-filters-opened');
    });

    $('.float-label .form-control').on('change input focus click blur', function() {
        var $parent = $(this).closest('.form-group');

        if (this.value != '' || $(this).is(':focus')) {
            if (!$parent.hasClass('input-filled')) {
                $parent.addClass('input-filled');
            }
        } else {
            if ($parent.hasClass('input-filled')) {
                $parent.removeClass('input-filled');
            }
        }
    });

    setTimeout(function() {
        $('.animate').addClass('animated');
    }, 100);

    (function($container) {
        $container.find('.category-menu-item').hover(
            function() {
                if (window.innerWidth <= 767) {
                    return;
                }

                $(this).children('.submenu').stop().fadeIn(200);
            },
            function() {
                if (window.innerWidth <= 767) {
                    return;
                }

                $(this).children('.submenu').stop().fadeOut(200);
            },
        );

        $container.find('.category-menu-item > a').click(function(e) {
            if (window.innerWidth > 767) {
                return;
            }

            if ($(document.body).hasClass('menu-opened')) {
                e.preventDefault();
            } else {
                return;
            }

            $(this).parent().addClass('active').siblings('.category-menu-item').removeClass('active');

            var level = $(this).parents('.submenu').length + 1;
            $container.css('transform', 'translate(-' + level * 100 + '%, 0)');
        });

        $container.find('.submenu .previous').click(function(e) {
            var level = $(this).parents('.submenu').length - 1;
            $container.css('transform', 'translate(-' + level * 100 + '%, 0)');
        });
    })($('.head-menu .blocks-container'));

    $('.modal#employee').on('show.bs.modal', function(e) {
        var $item = $(e.relatedTarget).closest('.employee-item'),
            $self = $(this).find('.modal-content');

        $self.find('.modal-title').html($item.children('.name').html());
        $self.find('.role').html($item.children('.text-muted').html());
        $self.find('.user-content').html($item.children('.user-content').html());
        $self.find('.image').html($item.children('img').get(0).outerHTML);
    });
});

/*! jQuery & Zepto Lazy v1.7.10 - http://jquery.eisbehr.de/lazy - MIT&GPL-2.0 license - Copyright 2012-2018 Daniel 'Eisbehr' Kern */
!function(t,e){"use strict";function r(r,a,i,u,l){function f(){L=t.devicePixelRatio>1,i=c(i),a.delay>=0&&setTimeout(function(){s(!0)},a.delay),(a.delay<0||a.combined)&&(u.e=v(a.throttle,function(t){"resize"===t.type&&(w=B=-1),s(t.all)}),u.a=function(t){t=c(t),i.push.apply(i,t)},u.g=function(){return i=n(i).filter(function(){return!n(this).data(a.loadedName)})},u.f=function(t){for(var e=0;e<t.length;e++){var r=i.filter(function(){return this===t[e]});r.length&&s(!1,r)}},s(),n(a.appendScroll).on("scroll."+l+" resize."+l,u.e))}function c(t){var i=a.defaultImage,o=a.placeholder,u=a.imageBase,l=a.srcsetAttribute,f=a.loaderAttribute,c=a._f||{};t=n(t).filter(function(){var t=n(this),r=m(this);return!t.data(a.handledName)&&(t.attr(a.attribute)||t.attr(l)||t.attr(f)||c[r]!==e)}).data("plugin_"+a.name,r);for(var s=0,d=t.length;s<d;s++){var A=n(t[s]),g=m(t[s]),h=A.attr(a.imageBaseAttribute)||u;g===N&&h&&A.attr(l)&&A.attr(l,b(A.attr(l),h)),c[g]===e||A.attr(f)||A.attr(f,c[g]),g===N&&i&&!A.attr(E)?A.attr(E,i):g===N||!o||A.css(O)&&"none"!==A.css(O)||A.css(O,"url('"+o+"')")}return t}function s(t,e){if(!i.length)return void(a.autoDestroy&&r.destroy());for(var o=e||i,u=!1,l=a.imageBase||"",f=a.srcsetAttribute,c=a.handledName,s=0;s<o.length;s++)if(t||e||A(o[s])){var g=n(o[s]),h=m(o[s]),b=g.attr(a.attribute),v=g.attr(a.imageBaseAttribute)||l,p=g.attr(a.loaderAttribute);g.data(c)||a.visibleOnly&&!g.is(":visible")||!((b||g.attr(f))&&(h===N&&(v+b!==g.attr(E)||g.attr(f)!==g.attr(F))||h!==N&&v+b!==g.css(O))||p)||(u=!0,g.data(c,!0),d(g,h,v,p))}u&&(i=n(i).filter(function(){return!n(this).data(c)}))}function d(t,e,r,i){++z;var o=function(){y("onError",t),p(),o=n.noop};y("beforeLoad",t);var u=a.attribute,l=a.srcsetAttribute,f=a.sizesAttribute,c=a.retinaAttribute,s=a.removeAttribute,d=a.loadedName,A=t.attr(c);if(i){var g=function(){s&&t.removeAttr(a.loaderAttribute),t.data(d,!0),y(T,t),setTimeout(p,1),g=n.noop};t.off(I).one(I,o).one(D,g),y(i,t,function(e){e?(t.off(D),g()):(t.off(I),o())})||t.trigger(I)}else{var h=n(new Image);h.one(I,o).one(D,function(){t.hide(),e===N?t.attr(C,h.attr(C)).attr(F,h.attr(F)).attr(E,h.attr(E)):t.css(O,"url('"+h.attr(E)+"')"),t[a.effect](a.effectTime),s&&(t.removeAttr(u+" "+l+" "+c+" "+a.imageBaseAttribute),f!==C&&t.removeAttr(f)),t.data(d,!0),y(T,t),h.remove(),p()});var m=(L&&A?A:t.attr(u))||"";h.attr(C,t.attr(f)).attr(F,t.attr(l)).attr(E,m?r+m:null),h.complete&&h.trigger(D)}}function A(t){var e=t.getBoundingClientRect(),r=a.scrollDirection,n=a.threshold,i=h()+n>e.top&&-n<e.bottom,o=g()+n>e.left&&-n<e.right;return"vertical"===r?i:"horizontal"===r?o:i&&o}function g(){return w>=0?w:w=n(t).width()}function h(){return B>=0?B:B=n(t).height()}function m(t){return t.tagName.toLowerCase()}function b(t,e){if(e){var r=t.split(",");t="";for(var a=0,n=r.length;a<n;a++)t+=e+r[a].trim()+(a!==n-1?",":"")}return t}function v(t,e){var n,i=0;return function(o,u){function l(){i=+new Date,e.call(r,o)}var f=+new Date-i;n&&clearTimeout(n),f>t||!a.enableThrottle||u?l():n=setTimeout(l,t-f)}}function p(){--z,i.length||z||y("onFinishedAll")}function y(t,e,n){return!!(t=a[t])&&(t.apply(r,[].slice.call(arguments,1)),!0)}var z=0,w=-1,B=-1,L=!1,T="afterLoad",D="load",I="error",N="img",E="src",F="srcset",C="sizes",O="background-image";"event"===a.bind||o?f():n(t).on(D+"."+l,f)}function a(a,o){var u=this,l=n.extend({},u.config,o),f={},c=l.name+"-"+ ++i;return u.config=function(t,r){return r===e?l[t]:(l[t]=r,u)},u.addItems=function(t){return f.a&&f.a("string"===n.type(t)?n(t):t),u},u.getItems=function(){return f.g?f.g():{}},u.update=function(t){return f.e&&f.e({},!t),u},u.force=function(t){return f.f&&f.f("string"===n.type(t)?n(t):t),u},u.loadAll=function(){return f.e&&f.e({all:!0},!0),u},u.destroy=function(){return n(l.appendScroll).off("."+c,f.e),n(t).off("."+c),f={},e},r(u,l,a,f,c),l.chainable?a:u}var n=t.jQuery||t.Zepto,i=0,o=!1;n.fn.Lazy=n.fn.lazy=function(t){return new a(this,t)},n.Lazy=n.lazy=function(t,r,i){if(n.isFunction(r)&&(i=r,r=[]),n.isFunction(i)){t=n.isArray(t)?t:[t],r=n.isArray(r)?r:[r];for(var o=a.prototype.config,u=o._f||(o._f={}),l=0,f=t.length;l<f;l++)(o[t[l]]===e||n.isFunction(o[t[l]]))&&(o[t[l]]=i);for(var c=0,s=r.length;c<s;c++)u[r[c]]=t[0]}},a.prototype.config={name:"lazy",chainable:!0,autoDestroy:!0,bind:"load",threshold:500,visibleOnly:!1,appendScroll:t,scrollDirection:"both",imageBase:null,defaultImage:"data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==",placeholder:null,delay:-1,combined:!1,attribute:"data-src",srcsetAttribute:"data-srcset",sizesAttribute:"data-sizes",retinaAttribute:"data-retina",loaderAttribute:"data-loader",imageBaseAttribute:"data-imagebase",removeAttribute:!0,handledName:"handled",loadedName:"loaded",effect:"show",effectTime:0,enableThrottle:!0,throttle:250,beforeLoad:e,afterLoad:e,onError:e,onFinishedAll:e},n(t).on("load",function(){o=!0})}(window);
