<?php

return [
    'caption' => 'Информация о компании',
    'settings' => [
        'company_logo' => [
            'caption' => 'Логотип',
            'type' => 'image',
            'note' => 'Если не указан, будет использован логотип по умолчанию',
        ],
        'company_logo_white' => [
            'caption' => 'Логотип (белый)',
            'type' => 'image',
        ],
        'favicon' => [
            'caption' => 'Фавикон',
            'type' => 'image',
        ],
        'company_address' => [
            'caption' => 'Адрес компании',
            'type'  => 'text',
        ],
        'company_phone' => [
            'caption' => 'Контактный телефон компании',
            'type'  => 'text',
        ],
        'company_email' => [
            'caption' => 'Контактный email компании',
            'type'  => 'text',
        ],
        'head_text' => [
            'caption' => 'Текст в шапке',
            'type'  => 'textareamini',
        ],
        'consultation_text' => [
            'caption' => 'Текст в футере',
            'type'  => 'textareamini',
        ],
        'policy' => [
            'caption' => 'Политика конфиденциальности',
            'type' => 'file',
        ],
        'images_alg' => [
            'caption' => 'Обработка изображений',
            'type' => 'dropdown',
            'elements' => 'Обрезать==0||Вписать (с белыми краями)==1',
            'default_value' => 0,
        ],
    ],
];
