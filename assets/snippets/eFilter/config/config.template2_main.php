<?php
if(!defined('MODX_BASE_PATH')){die('What are you doing? Get out of here!');}

include __DIR__ . '/config.template1.php';

//общая форма фильтра
$tplFilterForm = '
	<div class="section">
		<div class="container">
			<div class="filter main">
				<form id="eFiltr" action="[~2~]" method="get">
					[+wrapper+]

					<div class="text-xs-center">
						<button type="submit" class="btn btn-theme" id="main-submit" data-url="[~14~]">Подобрать</button>
					</div>
				</form>
			</div>
		</div>
	</div>
';

//кнопка "сброса" фильтра
$tplFilterReset = '';

//чекбоксы
$tplRowCheckbox = '
	<label class="[+disabled+]">
		<input type="checkbox" name="f[[+tv_id+]][]" value="[+value+]" [+selected+] [+disabled+]> [+name+]
	</label><br>
';

$filterCatName = '';

$tplOuterCategory = '<div class="eFiltr_cat eFiltr_cat[+iteration+]">
        [+wrapper+]
    </div>';
