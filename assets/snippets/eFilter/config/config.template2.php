<?php

include __DIR__ . '/config.default.php';

if(!defined('MODX_BASE_PATH')){die('What are you doing? Get out of here!');}

//общая форма фильтра
$tplFilterForm = '<div class="filter side"><form id="eFiltr" action="[+url+]" method="get">[+wrapper+]</form></div>';

//кнопка "сброса" фильтра
$tplFilterReset = '<div class="filter-side-buttons text-xs-right"><a href="[+reset_url+]" class="btn btn-hollow-theme">Сбросить фильтр</a></div>';

//название категории фильтра
$filterCatName = '<div class="filter-group">[+cat_name+]</div>';


//чекбоксы
$tplRowCheckbox = '
	<label class="[+disabled+]">
		<input type="checkbox" name="f[[+tv_id+]][]" value="[+value+]" [+selected+] [+disabled+]> [+name+] <span class="fltr_count">([+count+])</span>
	</label><br>
';
$tplOuterCheckbox = '
	<div class="filter-option fltr_block_checkbox fltr_block[+tv_id+]">
		<div class="filter-name fltr_name_checkbox fltr_name[+tv_id+]">[+name+]</div>
		<div class="checks">
			[+wrapper+]
		</div>
	</div>
';


//выпадающий список - селект
$tplRowSelect = '<option value="[+value+]" [+selected+] [+disabled+]>[+name+] ([+count+])</option>';
$tplOuterSelect = '
	<div class="filter-option fltr_block_select fltr_block[+tv_id+]">
		<div class="filter-name fltr_name_select fltr_name[+tv_id+]">[+name+]</div>
		<select name="f[[+tv_id+]][]" class="form-control">
			<option value="0"> - [+name+] - </option>
			[+wrapper+]
		</select>
	</div>
';


//диапазон
$tplRowInterval = 'от<input type="text" name="f[[+tv_id+]][min]" value="[+minval+]" data-min-val="[+minvalcurr+]"> до <input type="text" name="f[[+tv_id+]][max]" value="[+maxval+]" data-max-val="[+maxvalcurr+]">';
$tplOuterInterval = '
	<div class="filter-option fltr_block_interval fltr_block[+tv_id+]">
		<div class="filter-name fltr_name_interval fltr_name[+tv_id+]">[+name+]</div>
		[+wrapper+]
	</div>
';


//радио - radio 
$tplRowRadio = '<input type="radio" name="f[[+tv_id+]][]" value="[+value+]" [+selected+] [+disabled+]> [+name+] <span class="fltr_count">[+count+]</span>';
$tplOuterRadio = '
	<div class="filter-option fltr_block_radio fltr_block[+tv_id+]">
		<div class="filter-name fltr_name_radio fltr_name[+tv_id+]">[+name+]</div>
		<input type="radio" name="f[[+tv_id+]][]" value="0"> Все
		[+wrapper+]
	</div>
';

//выпадающий список - мультиселект
$tplRowMultySelect = '<option value="[+value+]" [+selected+] [+disabled+]>[+name+] ([+count+])</option>';
$tplOuterMultySelect = '
	<div class="filter-option fltr_block_multy fltr_block[+tv_id+]">
		<div class="filter-name fltr_name_multy fltr_name[+tv_id+]">[+name+]</div>
		<select name="f[[+tv_id+]][]" multiple size="5" class="form-control">
			<option value="0"> - [+name+] - </option>
			[+wrapper+]
		</select>
	</div>
';

//слайдер
//слайдер
$tplRowSlider = '
	<div class="value">
		<input type="text" name="f[[+tv_id+]][min]" value="[+minval+]" id="minCostInp[+tv_id+]" class="form-control fltr_min" data-min-val="[+minvalcurr+]">
	</div><div class="value">
		<input type="text" name="f[[+tv_id+]][max]" value="[+maxval+]" id="maxCostInp[+tv_id+]" class="form-control fltr_max" data-max-val="[+maxvalcurr+]">
	</div>
';
$tplOuterSlider = <<< TPL_OUTER_SLIDER
	<div class="filter-option fltr_block_slider fltr_block[+tv_id+]">
		<div class="filter-name fltr_name_slider fltr_name[+tv_id+]">[+name+]</div>
		<div class="fltr_inner fltr_inner_slider fltr_inner[+tv_id+]">
			[+wrapper+]
			<div class="value-limits slider_text[+tv_id+]">
				<span class="minCost pull-xs-left"></span> 
				<span class="maxCost pull-xs-right"></span>
			</div>
			<div class="filter-slider">
				<div class="fltr_slider"></div>
			</div>
		</div>

		<script type="text/javascript">
			$(document).ready(function(){
				$('.fltr_block[+tv_id+] .fltr_slider').each(function(i, elem) {
	                var block = $(elem).closest('.fltr_block_slider');

	                var minObj = block.find('[data-min-val]');
	                var maxObj = block.find('[data-max-val]');

	                var min = parseInt(minObj.attr('data-min-val')) || 0;
	                var max = parseInt(maxObj.attr('data-max-val')) || 0;

	                block.find('.minCost').text(min);
	                block.find('.maxCost').text(max);

	                min = parseInt(minObj.val()) || min;
	                max = parseInt(maxObj.val()) || max;

	                minObj.val(min);
	                maxObj.val(max);

	                $(elem).slider({
	                    min: parseInt(minObj.data('min-val')),
	                    max: parseInt(maxObj.data('max-val')),
	                    values: [min, max],
	                    range: true,

	                    stop: function(e, ui) {
	                        minObj.val(ui.values[0]);
	                        maxObj.val(ui.values[1]);

	                        minObj.change();
	                    },

	                    slide: function(e, ui){
	                        minObj.val(ui.values[0]);
	                        maxObj.val(ui.values[1]);
	                    }
	                });
	            });
			});
		</script>
	</div>

TPL_OUTER_SLIDER;

//цвета
$tplRowColors = '
	<label class="[+disabled+] [+label_selected+]" style="background:[+value+]" title="[+name+] ([+count+])">
		<input type="checkbox" name="f[[+tv_id+]][]" value="[+value+]" [+selected+] [+disabled+]> [+name+] <span class="fltr_count">[+count+]</span>
	</label>
';
$tplOuterColors = '
	<div class="filter-option fltr_block_checkbox fltr_colors fltr_block[+tv_id+] fltr_colors[+tv_id+]">
		<div class="filter-name fltr_name_checkbox fltr_name[+tv_id+]">[+name+]</div>
		[+wrapper+]
	</div>
';

//паттерн
$tplRowPattern = '
	<label class="[+disabled+] [+label_selected+]" title="[+name+] ([+count+])">
		<input type="checkbox" name="f[[+tv_id+]][]" value="[+value+]" [+selected+] [+disabled+]> <img src="[+pattern_folder+][+value+]" alt="[+name+]"> [+name+] <span class="fltr_count">[+count+]</span>
	</label>
';
$tplOuterPattern = '
	<div class="filter-option fltr_block_checkbox fltr_pattern fltr_block[+tv_id+] fltr_pattern[+tv_id+]">
		<div class="filter-name fltr_name_checkbox fltr_name[+tv_id+]">[+name+]</div>
		[+wrapper+]
	</div>
';

