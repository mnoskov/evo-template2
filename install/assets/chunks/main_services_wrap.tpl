/**
 * main_services_wrap
 *
 * main_services_wrap
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="main-services-list">
    <ul class="items">
        [+dl.wrap+]
    </ul>
</div>
