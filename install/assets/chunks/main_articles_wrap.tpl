/**
 * main_articles_wrap
 *
 * main_articles_wrap
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="main-articles-list">
    <ul class="items">
        [+dl.wrap+]
    </ul>
</div>
