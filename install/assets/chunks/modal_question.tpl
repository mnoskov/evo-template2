/**
 * modal_question
 *
 * modal_question
 *
 * @category    chunk
 * @internal    @modx_category Формы
 * @internal    @overwrite true
*/
<div class="modal fade" tabindex="-1" role="dialog" id="question">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="#" class="ajax" data-goal="form:zakaz">
                <button type="button" class="close icon-close" data-dismiss="modal"></button>

                <div class="modal-title">
                    Задайте нам вопрос
                </div>

                <div class="modal-body">
                    <div class="form-group wi float-label">
                        <label>
                            <span>Ваш вопрос *</span>
                        </label>

                        <textarea name="msg" class="form-control"></textarea>
                        <i class="icon-pencil"></i>
                    </div>

                    <div class="form-group wi float-label">
                        <label>
                            <span>Ваше имя</span>
                        </label>

                        <input type="text" name="name" class="form-control">
                        <i class="icon-user"></i>
                    </div>

                    <div class="form-group wi float-label">
                        <label>
                            <span>Контактый телефон *</span>
                        </label>

                        <input type="text" name="phone" class="mask-phone form-control">
                        <i class="icon-phone"></i>
                    </div>

                    {{policy_note}}

                    <input type="hidden" name="pid" value="[*id*]">
                    <input type="hidden" name="formid" value="question">
                    <button type="submit" class="btn btn-theme">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</div>
