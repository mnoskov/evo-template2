/**
 * main_instagram_wrap
 *
 * main_instagram_wrap
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="instagram-list">
    <ul class="items">
        [+dl.wrap+]
    </ul>
</div>
