/**
 * main_articles_item
 *
 * main_articles_item
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<li class="[+customclass+]">
    <div class="item">
        <div class="image">
            <a href="[+url+]">
                <span style="background-color: [[getImageColor? &source=`[+tv.image+]`]];" class="lazy" data-src="[[phpthumb? &input=`[+tv.image+]` &options=`[+imagesize+],f=jpg,zc=1` &adBlockFix=`1`]]"></span>
            </a>
        </div>

        <div class="info">
            <div class="title">
                <a href="[+url+]">
                    [+pagetitle+]
                </a>
            </div>

            <div class="intro">
                [+introtext+]
            </div>
        </div>

        <div class="date">
            [[formatDate? &in=`[+pub_date+]`]]
        </div>
    </div>
