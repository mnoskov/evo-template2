/**
 * head_menu_parent_item
 *
 * head_menu_parent_item
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<li class="category-menu-item [+classNames+]">
    <a href="[+url+]" title="[+e.title+]">
        <span>[+title+]<i class="icon-small-down-arrow"></i></span>
    </a>

    <div class="submenu">
        <span class="previous">
            <span class="arrow">&larr;</span>
            Назад
        </span>

        <a href="[+url+]" title="[+e.title+]" class="parent-link hidden-md-up">
            <span>[+title+]</span>
        </a>

        [+wrap+]
    </div>
</li>
