/**
 * reviews_cycle
 *
 * reviews_cycle
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="reviews-cycle">
    <div class="slick" data-slick='{"dots": true, "arrows": false, "slidesToShow": 2, "responsive": [{"breakpoint": 767, "settings": {"slidesToShow": 1}}]}'>
        [+dl.wrap+]
    </div>
</div>
