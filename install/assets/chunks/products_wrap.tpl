/**
 * products_wrap
 * 
 * products_wrap
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="products-list">
    <div class="items" id="eFiltr_results">
        [+dl.wrap+]
        [+pages+]
    </div>
</div>