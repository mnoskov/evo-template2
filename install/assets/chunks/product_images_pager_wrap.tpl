/**
 * product_images_pager_wrap
 * 
 * product_images_pager_wrap
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="thumbs slick" data-slick='{ "slidesToShow": 5, "swipeToSlide": true, "asNavFor": ".images", "focusOnSelect": true, "arrows": false, "responsive": [ { "breakpoint": 576, "settings": { "slidesToShow": 3 } } ] }'>
    [+dl.wrap+]
</div>