/**
 * footer
 *
 * footer
 *
 * @category    chunk
 * @internal    @overwrite true
*/

    </div>

    <div class="consultation">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="title">Нужна консультация?</div>
                </div>

                <div class="col-md-6 col-xl-7">
                    [(client_consultation_text)]<br>
                </div>

                <div class="col-md-2 text-md-right text-left">
                    <a href="#" class="btn btn-hollow-white" data-toggle="modal" data-target="#question">Задать вопрос</a>
                </div>
            </div>
        </div>
    </div>

    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 col-lg-3">
                    <div class="copy hidden-sm-down">
                        © [!year!] Все права защищены.
                    </div>
                    [[renderSocials]]
                    <p>
                        <a href="[(client_policy)]">Политика конфиденциальности</a>
                    </p>
                    <p>
                        <a href="https://trylike.ru/" target="_blank">Разработка сайта - Like Marketing</a>
                    </p>
                </div>

                <div class="col-md-8 col-lg-9">
                    <div class="row">
                        <div class="col-lg-8 col-xl-9 hidden-xs-down">
                            <div class="bottom-menu">
                                [[DLMenu?
                                    &parents=`1`
                                    &maxDepth=`2`
                                    &outerTpl=`head_menu_wrap`
                                    &rowTpl=`head_menu_item`
                                    &addWhereList=`c.id NOT IN (2) AND c.parent NOT IN (26) AND c.hidemenu != 1`
                                ]]
                            </div>
                        </div>

                        <div class="col-lg-4 col-xl-3" itemscope itemtype="http://schema.org/Organization">
                            <span class="hidden" itemprop="name">[(site_name)]</span>

                            <div class="wi-group phone">
                                <i class="icon-phone"></i>
                                <span itemprop="telephone">[[splitRows? &in=`[(client_company_phone)]` &tpl=`phones_row`]]</span>
                                <a href="#" class="callback" data-toggle="modal" data-target="#callback">Заказать звонок</a>
                            </div>

                            <div class="wi-group" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                <i class="icon-map-pin"></i>
                                <span itemprop="streetAddress">[(client_company_address)]</span>
                            </div>

                            <div class="wi-group">
                                <a href="mailto:[(client_company_email)]">
                                    <i class="icon-email"></i>
                                    <span itemprop="email">[(client_company_email)]</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 hidden-md-up">
                    <div class="copy last">
                        © [!year!] Все права защищены.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popups">
        {{modal_callback}}
        {{modal_question}}
        {{modal_order}}
        {{modal_employee}}
        <div class="modal fade" tabindex="-1" role="dialog" id="response">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close icon-close" data-dismiss="modal"></button>
                        <div class="response"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nstslider/1.0.13/jquery.nstSlider.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-animateNumber/0.0.14/jquery.animateNumber.min.js"></script>
    <script type="text/javascript" src="assets/templates/default/js/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/templates/default/js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="assets/templates/default/js/jquery.flextabs.min.js"></script>
    <script type="text/javascript" src="assets/templates/default/js/common.js"></script>
    <script type="text/javascript" src="assets/templates/default/js/template.js"></script>
    [!IgnoreLighthouse? &content=`[(client_body_end_scripts)]`!]
</body>
</html>
