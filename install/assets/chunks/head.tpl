/**
 * head
 *
 * head
 *
 * @category    chunk
 * @internal    @overwrite true
*/
{{header}}
    <div class="wrap">
        <div class="head-placeholder static">
            <div class="head">
                <div class="container">
                    <div class="blocks-container">
                        <div class="left-block">
                            <div class="blocks-container">
                                <div class="toggle-menu hidden-md-up">
                                    <i class="icon-menu"></i>
                                </div>

                                <div class="logo">
                                    <a href="/">
                                        [[if? &is=`[(client_company_logo)]:!empty` &then=`
                                            <img src="[(client_company_logo)]" alt="[(site_name)]" class="colored">
                                            <img src="[(client_company_logo_white)]" alt="[(site_name)]" class="white">
                                        ` &else=`
                                            {{logo}}
                                        `]]
                                    </a>
                                </div>

                                <div class="show-on-top hidden-md-down text">
                                    [[nl2br? &in=`[(client_head_text)]`]]
                                </div>
                            </div>
                        </div>

                        <div class="right-block">
                            <div class="blocks-container">
                                <div class="contacts">
                                    <div class="blocks-container">
                                        <div class="address">
                                            <div class="wi-group">
                                                <i class="icon-map-pin"></i>
                                                [(client_company_address)]
                                            </div>
                                        </div>

                                        <div class="phone">
                                            <div class="wi-group">
                                                <i class="icon-phone"></i>
                                                [[splitRows? &in=`[(client_company_phone)]` &tpl=`phones_row`]]
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="callback">
                                    <a href="#" data-toggle="modal" data-target="#callback" class="btn btn-hollow-theme">
                                        Заказать звонок
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="head-menu">
                <div class="menu-overlay"></div>

                <div class="container">
                    <div class="blocks-container">
                        <div class="logo">
                            <a href="/">
                                [[if? &is=`[(client_company_logo)]:!empty` &then=`
                                    <img src="[(client_company_logo)]" alt="[(site_name)]">
                                ` &else=`
                                    {{logo}}
                                `]]
                            </a>
                        </div>

                        <div class="menu">
                            [[DLMenu?
                                &parents=`1`
                                &maxDepth=`2`
                                &outerTpl=`head_menu_wrap`
                                &parentRowTpl=`head_menu_parent_item`
                                &rowTpl=`head_menu_item`
                                &addWhereList=`c.parent NOT IN (26) AND c.hidemenu != 1`
                            ]]
                        </div>

                        <div class="toggle-search-wrap">
                            <div class="toggle-search">
                                <i class="icon-search"></i>
                                <span class="hidden-md-up">Поиск</span>
                            </div>
                        </div>

                        <div class="hidden-content">
                            <div class="address">
                                <div class="wi-group">
                                    <i class="icon-map-pin"></i>
                                    [(client_company_address)]
                                </div>
                            </div>

                            <div class="phone">
                                <div class="wi-group">
                                    <i class="icon-phone"></i>
                                    [[splitRows? &in=`[(client_company_phone)]` &tpl=`phones_row`]]
                                </div>
                            </div>

                            [[renderSocials]]
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="search-wrap">
            <div class="container">
                <div class="search-form">
                    <form action="[~51~]" method="get">
                        <div class="blocks-container">
                            <div class="form">
                                <input type="text" name="search" class="form-control" placeholder="Поиск"><button type="submit"><i class="icon-search"></i></button>
                            </div>

                            <div class="toggle-search">
                                <i class="icon-close"></i>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
