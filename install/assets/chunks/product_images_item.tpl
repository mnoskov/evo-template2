/**
 * product_images_item
 * 
 * product_images_item
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="slide slick-slide">
    <a href="[[phpthumb? &input=`[+sg_image+]` &options=`w=1600,h=1600,f=jpg` &adBlockFix=`1`]]" data-fancybox="images" data-caption="[+e.sg_description+]" style="background-image: url('[[thumb? &input=`[+sg_image+]` &options=`w=635,h=350,f=jpg`]]');"></a>
</div>
