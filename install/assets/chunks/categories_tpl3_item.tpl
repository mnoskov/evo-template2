/**
 * categories_tpl3_item
 *
 * categories_tpl3_item
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<li>
    <div class="item">
        <div class="image">
            <a href="[+url+]">
                <img src="[[getImageColor? &source=`[+tv.image+]` &output=`image` &width=`120` &height=`120`]]" data-src="[[thumb? &input=`[+tv.image+]` &options=`w=120,h=120,f=jpg`]]" class="lazy img-fluid">
            </a>
        </div>

        <div class="info">
            <div class="title">
                <a href="[+url+]">[+pagetitle+]</a>
            </div>

            [+children+]

            <div class="intro">
                [[nl2br? &in=`[+introtext+]`]]
            </div>
        </div>
    </div>
