/**
 * reviews_wrap
 *
 * reviews_wrap
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="reviews-list">
    [+dl.wrap+]
    [+pages+]
</div>
