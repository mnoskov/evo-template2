/**
 * page_head_default
 *
 * page_head_default
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="page-head default">
    <div class="container">
        [+before_content+]
        [+before_title+]

        <h1 class="page-title">
            [[metatitle? &appendSiteName=`0`]]
        </h1>

        {{breadcrumbs}}
        [+after_title+]
        [+after_content+]
    </div>
</div>
