/**
 * product_images_wrap
 * 
 * product_images_wrap
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="images slick" data-slick='{"asNavFor": ".thumbs"}'>
    [+dl.wrap+]
</div>
