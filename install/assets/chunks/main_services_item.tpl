/**
 * main_services_item
 *
 * main_services_item
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<li class="[+customclass+]">
    <div class="item">
        <div class="image lazy" style="background-color: [[getImageColor? &source=`[+tv.image+]`]]" data-src="[[thumb? &input=`[+tv.image+]` &options=`[+imagesize+],f=jpg`]]"></div>

        <div class="info">
            <div class="title">
                <span class="inline-padding"><span><span>[+pagetitle+]</span></span></span>
            </div>

            <div class="intro">
                [[nl2br? &in=`[+introtext+]`]]
            </div>
        </div>

        <a href="[+url+]"></a>
    </div>
