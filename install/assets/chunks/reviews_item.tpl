/**
 * reviews_item
 *
 * reviews_item
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="review-item">
    <div class="row">
        <div class="col-sm-3">
            <div class="image">
                <a href="[+url+]"><img src="[[phpthumb? &input=`[+tv.image+]` &options=`w=178,h=178,zc=1` &adBlockFix=`1`]]" alt="[+e.pagetitle+]" class="img-fluid"></a>
            </div>
        </div>

        <div class="col-sm-9">
            <div class="title">
                <a href="[+url+]">[+pagetitle+]</a>
            </div>

            <div class="text-muted">
                [+introtext+]
            </div>

            <div class="user-content">
                [[smartCrop? &str=`[+content+]` &length=`250`]]
            </div>
        </div>
    </div>
</div>
