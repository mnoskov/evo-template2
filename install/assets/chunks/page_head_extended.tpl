/**
 * page_head_extended
 *
 * page_head_extended
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="page-head extended" style="background-image: url('[*head_background*]'); background-color: [[getImageColor? &source=`[*head_background*]`]]; color: [[if? &is=`[*head_text_color*]:!empty` &then=`[*head_text_color*]` &else=`[[getImageColor? &source=`[*head_background*]` &contrast=`1` &dark=`#333`]]`]];">
    <div class="container">
        [+before_content+]

        <div class="row">
            <div class="col-lg-5 col-xl-6">
                <div class="animate page-head-content">
                    [+before_title+]

                    <h1 class="page-title">
                        [[metatitle? &appendSiteName=`0`]]
                    </h1>

                    {{breadcrumbs}}

                    <div class="user-content">
                        [*introtext*]
                    </div>

                    [+after_title+]
                </div>
            </div>
        </div>

        [+after_content+]
    </div>

    <div class="image">
        <div class="container">
            <div style="background-image: url('[*head_image*]');"></div>
        </div>
    </div>
</div>
