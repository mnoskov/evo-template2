/**
 * modal_employee
 *
 * modal_employee
 *
 * @category    chunk
 * @internal    @modx_category Формы
 * @internal    @overwrite true
*/
<div class="modal fade" tabindex="-1" role="dialog" id="employee">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close icon-close" data-dismiss="modal"></button>
            <div class="modal-title"></div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="image"></div>
                    </div>

                    <div class="col-sm-8">
                        <div class="role"></div>
                        <div class="user-content"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
