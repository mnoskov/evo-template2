/**
 * page_default_head_image
 * 
 * page_default_head_image
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
[[if? &is=`[*head_image*]~!empty~and~[*head_type*]~!=~_extended` &separator=`~` &then=`
    <div class="page-image full-width">
        <div style="background-image: url('[[phpthumb? &input=`[*head_image*]` &options=`w=897,h=331,zc=1,f=jpg,bg=FFFFFF` &adBlockFix=`1`]]');"></div>
    </div>
`]]
