/**
 * products_item
 *
 * products_item
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="slide">
    <div class="item">
        <a href="[+url+]" class="image">
            <span style="background-image: url('[[thumb? &input=`[+tv.image+]` &options=`w=353,h=261,f=jpg`]]');"></span>
        </a>

        [[renderProductLabels? &docid=`[+id+]`]]

        <div class="info">
            <div class="title">
                <a href="[+url+]">[+pagetitle+]</a>
            </div>

            <div class="user-content">
                [+description+]
            </div>

            <div class="price">
                [[formatPrice? &in=`[+tv.price+]`]]
            </div>

            [[if? &is=`[+tv.old_price+]:!empty` &then=`
                <span class="old-price">
                    [[formatPrice? &in=`[+tv.old_price+]`]]
                </span>
            `]]
        </div>
    </div>
</div>
