/**
 * left_menu
 * 
 * left_menu
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
[[DLMenu? 
    &parents=`[[UltimateParent? &topLevel=`2`]]` 
    &maxDepth=`2` 
    &outerTpl=`@CODE: <ul class="side-nav">[+wrap+]</ul>`
    &outerTpl1=`@CODE: <ul class="subnav">[+wrap+]</ul>`
    &rowTpl=`@CODE: <li[+classes+]><a href="[+url+]" title="[+e.title+]"><i class="icon-small-right-arrow"></i>[+title+]</a></li>`
    &parentRowTpl=`@CODE: <li[+classes+]><a href="[+url+]" title="[+e.title+]"><i class="icon-small-down-arrow"></i>[+title+]</a>[+wrap+]</li>`
    &rowTpl1=`@CODE: <li[+classes+]><a href="[+url+]" title="[+e.title+]">[+title+]</a></li>`
    &hideSubMenus=`1`
]]
