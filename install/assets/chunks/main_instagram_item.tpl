/**
 * main_instagram_item
 *
 * main_instagram_item
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<li>
    <div class="item">
        <div class="image" style="background-image: url('[[phpthumb? &input=`[+image+]` &options=``]]');"></div>

        <div class="hidden scrollbar-inner">
            <a class="hidden-content" href="[+url+]" target="_blank" rel="nofollow">
                <span class="date">
                    [[formatDate? &in=`[+timestamp+]`]]
                </span>

                <span class="user-content">
                    [+caption+]
                </span>
            </a>
        </div>
    </div>
