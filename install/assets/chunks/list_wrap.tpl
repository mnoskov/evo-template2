/**
 * list_wrap
 * 
 * list_wrap
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="materials-list">
    [+dl.wrap+]
    [+pages+]
</div>