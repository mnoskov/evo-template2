/**
 * breadcrumbs
 * 
 * breadcrumbs
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
[[DLcrumbs?
    &showCurrent=`1`
    &addWhereList=`c.id != 1`
    &tpl=`@CODE:<a href="[+url+]" title="[+e.title+]">[+title+]</a><span class="separator">&mdash;</span>`
    &tplCurrent=`@CODE:<span class="current">[+title+]</span>`
    &ownerTPL=`@CODE:<div class="breadcrumbs">[+crumbs.wrap+]</div>`
]]
