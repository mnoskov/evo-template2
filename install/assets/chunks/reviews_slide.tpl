/**
 * reviews_slide
 *
 * reviews_slide
 *
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="slide">
    <div class="review-item review-slide">
        <div class="image">
            <a href="[+url+]"><img src="[[phpthumb? &input=`[+tv.image+]` &options=`w=178,h=178,zc=1` &adBlockFix=`1`]]" alt="[+e.pagetitle+]" class="img-fluid"></a>
        </div>

        <div class="review-head">
            <div class="title">
                <a href="[+url+]">[+pagetitle+]</a>
            </div>

            <div class="text-muted">
                [+introtext+]
            </div>
        </div>

        <div class="user-content">
            <i class="icon-quote"></i>
            [[smartCrop? &str=`[+content+]` &length=`250`]]
        </div>
    </div>
</div>
