/**
 * product_attributes_wrap
 * 
 * product_attributes_wrap
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="attributes">
    <table>
        <tbody>
            [+wrap+]
        </tbody>
    </table>
</div>
