/**
 * fixed_filters_form
 * 
 * fixed_filters_form
 * 
 * @category    chunk
 * @internal    @overwrite true
*/
<div class="fixed-filters-overlay"></div>
<div class="fixed-filters">
    <div class="toggle-fixed-filters hidden-md-up">
        <i class="icon-filter"></i>
    </div>

    <div class="filters-scrollable">
        [+form+]
    </div>
</div>
