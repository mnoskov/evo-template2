/**
 * article
 *
 * Статья
 *
 * @category template
 */
{{head}}
{{page_head[*head_type*]? &before_title=`<div class="date pull-xs-right">[[formatDate? &in=`[*pub_date*]`]]</div>`}}

<div class="page-layout">
    <div class="container">
        <div class="row">
            <div class="col-md-4 hidden-sm-down">
                <div class="left-column">
                    {{left_menu_articles}}
                </div>
            </div>

            <div class="col-xs-12 col-md-8">
                <div class="page-content">
                    {{page_default_head_image}}

                    [[if? &is=`[*content*]~~!empty` &separator=`~~` &then=`
                        <div class="user-content">
                            [*content*]
                        </div>
                    `]]
                </div>
            </div>
        </div>
    </div>
</div>

{{footer}}
