/**
 * gallery
 *
 * Галерея
 *
 * @category       template
 */
{{head}}
{{page_head[*head_type*]}}

<div class="page-layout">
    <div class="container">
        <div class="row">
            <div class="col-md-4 hidden-sm-down">
                <div class="left-column">
                    {{left_menu}}
                </div>
            </div>

            <div class="col-xs-12 col-md-8">
                <div class="page-content">
                    {{page_default_head_image}}

                    [[sgLister?
                        &parents=`[*id*]`
                        &tpl=`gallery_images_tpl`
                        &ownerTPL=`gallery_images_wrap`
                        &prepare=`prepareGalleryItem`
                        &prepareWrap=`prepareGalleryWrap`
                    ]]

                    [[if? &is=`[*content*]~~!empty` &separator=`~~` &then=`
                        <div class="user-content">
                            [*content*]
                        </div>
                    `]]
                </div>
            </div>
        </div>
    </div>
</div>

{{footer}}
