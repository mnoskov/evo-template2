/**
 * search
 *
 * Результаты поиска
 *
 * @category       template
 */
{{head}}
{{page_head[*head_type*]}}

<div class="page-layout">
    <div class="container">
        <div class="page-content">
            {{page_default_head_image}}

            [[evoSearch? 
                &id=`list`
                &tvPrefix=``
                &tpl=`@CODE:<p><a href="[+url+]">[+title+]</a></p><p>[+extract+]</p>` 
                &ownerTPL=`@CODE:<div class="search-results">[+dl.wrap+]<div class="text-xs-center">[+list.pages+]</div></div>` 
                &display=`10` 
                &paginate=`pages` 
                &orderBy=`pub_date DESC`
                &TplNextP=`@CODE: ` 
                &TplPrevP=`@CODE: ` 
                &TplCurrentPage=`@CODE:<span class="page current">[+num+]</span>` 
                &TplWrapPaginate=`@CODE:<div class="pagination">[+wrap+]</div>`
            ]]

            [[if? &is=`[*content*]~~!empty` &separator=`~~` &then=`
                <div class="user-content">
                    [*content*]
                </div>
            `]]
        </div>
    </div>
</div>

{{footer}}
