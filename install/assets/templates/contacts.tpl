/**
 * contacts
 *
 * Контакты
 *
 * @category template
 */
{{head}}
{{page_head[*head_type*]}}

<div class="page-layout">
    <div class="container">
        <div class="row">
            <div class="col-md-4 hidden-sm-down">
                <div class="left-column">
                    <div class="wi-group">
                        <i class="icon-map-pin"></i>
                        <strong>Адрес:</strong><br>
                        [(client_company_address)]
                    </div>

                    <div class="wi-group">
                        <i class="icon-phone"></i>
                        <strong>Контактный телефон:</strong><br>
                        [(client_company_phone)]
                    </div>

                    <div class="wi-group">
                        <strong>Email:</strong><br>
                        <a href="mailto:[(client_company_email)]">
                            <i class="icon-email"></i>
                            [(client_company_email)]
                        </a>
                    </div>

                    [[renderSocials?
                        &tpl=`@CODE:<div class="wi-group"><i class="icon-social-[+code+]"></i><strong>[+title+]:</strong><br><a href="[+url+]" title="[+title+]" target="_blank" rel="nofollow">[+url+]</a></div>`
                        &ownerTPL=`@CODE:[+wrap+]`
                    ]]
                </div>
            </div>

            <div class="col-xs-12 col-md-8">
                <div class="page-content">
                    <div class="map full-width">
                        [*map_code*]
                    </div>

                    <div class="contact-groups hidden-md-up">
                        <ul class="items">
                            <li>
                                <div class="wi-group">
                                    <i class="icon-map-pin"></i>
                                    <strong>Адрес:</strong><br>
                                    [(client_company_address)]
                                </div>

                            <li>
                                <div class="wi-group">
                                    <i class="icon-phone"></i>
                                    <strong>Контактный телефон:</strong><br>
                                    [(client_company_phone)]
                                </div>

                            <li>
                                <div class="wi-group">
                                    <strong>Email:</strong><br>
                                    <a href="mailto:[(client_company_email)]">
                                        <i class="icon-email"></i>
                                        [(client_company_email)]
                                    </a>
                                </div>

                            [[renderSocials?
                                &tpl=`@CODE:<li><div class="wi-group"><i class="icon-social-[+code+]"></i><strong>[+title+]:</strong><br><a href="[+url+]" title="[+title+]" target="_blank" rel="nofollow">[+url+]</a></div>`
                                &ownerTPL=`@CODE:[+wrap+]`
                            ]]
                        </ul>
                    </div>

                    [[if? &is=`[*content*]~~!empty` &separator=`~~` &then=`
                        <div class="user-content">
                            [*content*]
                        </div>
                    `]]
                </div>
            </div>
        </div>
    </div>
</div>

{{footer}}
