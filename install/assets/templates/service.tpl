/**
 * service
 *
 * Услуга
 *
 * @category       template
 */
{{head}}
{{page_head[*head_type*]? &after_title=`<a href="#" class="btn btn-hollow-white" data-toggle="modal" data-target="#service" style="margin-top: 1rem;">Заказать услугу</a>`}}

<div class="page-layout">
    <div class="container">
        <div class="row">
            <div class="col-md-4 hidden-sm-down">
                <div class="left-column">
                    {{left_menu}}
                </div>
            </div>

            <div class="col-xs-12 col-md-8">
                <div class="page-content">
                    {{page_default_head_image}}

                    [[if? &is=`[*content*]~~!empty` &separator=`~~` &then=`
                        <div class="user-content">
                            [*content*]
                        </div>
                    `]]

                    <div class="full-width">
                        <div class="service-footer">
                            <div class="blocks-container">
                                <div>
                                    <a href="#" class="btn btn-theme" data-toggle="modal" data-target="#service">
                                        Заказать услугу
                                    </a>
                                </div>

                                <div class="description">
                                    Оформите заявку на сайте. Наш менеджер свяжется с вами для уточнения деталей.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{modal_service}}
{{footer}}
