/**
 * review
 *
 * Отзыв
 *
 * @category template
 */
{{head}}
{{page_head[*head_type*]}}

<div class="page-layout">
    <div class="container">
        <div class="row">
            <div class="col-md-4 hidden-sm-down">
                <div class="left-column">
                    {{left_menu}}
                </div>
            </div>

            <div class="col-xs-12 col-md-8">
                <div class="page-content">
                    {{page_default_head_image}}

                    <div class="review-item">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="image">
                                    <img src="[[phpthumb? &input=`[*image*]` &options=`w=178,h=178,zc=1` &adBlockFix=`1`]]" class="img-fluid">
                                </div>
                            </div>

                            <div class="col-sm-9">
                                <div class="text-muted">
                                    [*introtext*]
                                </div>

                                <div class="user-content">
                                    [*content*]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{footer}}
