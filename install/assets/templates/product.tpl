/**
 * product
 *
 * Товар
 *
 * @category       template
 */
{{head}}
{{page_head[*head_type*]}}

<div class="page-layout">
    <div class="container">
        <div class="row">
            <div class="col-md-4 hidden-sm-down">
                <div class="left-column">
                    {{left_menu}}
                </div>
            </div>

            <div class="col-xs-12 col-md-8">
                <div class="page-content" itemscope itemtype="http://schema.org/Product">
                    <div class="hidden">
                        <span itemprop="name">[[metatitle? &appendSiteName=`0`]]</span>
                        <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                            <span itemprop="price">[*price*]</span>
                            <span itemprop="priceCurrency">RUB</span>
                        </span>
                    </div>

                    <div class="product-card">
                        <div class="row">
                            <div class="col-xl-7">
                                <div class="images-cycle">
                                    [[sgLister?
                                        &parents=`[*id*]`
                                        &tpl=`product_images_item`
                                        &ownerTPL=`product_images_wrap`
                                    ]]

                                    [[sgLister?
                                        &parents=`[*id*]`
                                        &tpl=`product_images_pager_item`
                                        &ownerTPL=`product_images_pager_wrap`
                                    ]]
                                </div>
                            </div>

                            <div class="col-xl-5">
                                <div class="info">
                                    <div class="price[[if? &is=`[*old_price*]:!empty` &then=` with-old-price`]]">
                                        <div class="value">
                                            [[formatPrice? &in=`[*price*]`]]
                                        </div>

                                        [[if? &is=`[*old_price*]:!empty` &then=`
                                            <div class="old-price">
                                                Без скидки:
                                                <span class="value">
                                                    [[formatPrice? &in=`[*old_price*]`]]
                                                </span>
                                            </div>
                                        `]]
                                    </div>

                                    <div class="user-content">
                                        [*introtext*]
                                    </div>

                                    <div class="buttons">
                                        <a href="#" class="btn btn-theme" data-toggle="modal" data-target="#order" data-set-product="[*pagetitle*]">Заказать</a>
                                        <a href="#" class="btn btn-hollow-theme" data-toggle="modal" data-target="#question" data-set-product="[*pagetitle*]">Задать вопрос</a>
                                    </div>

                                    [[share]]
                                </div>
                            </div>
                        </div>
                    </div>

                    [[PageBuilder? &container=`product_features`]]

                    <div class="tabs-wrapper full-width">
                        <div id="product-tabs" data-ft style="display: none;">
                            <nav>
                                [[if? &is=`[*content*]~~!empty` &separator=`~~` &then=`
                                    <a href="#description">Описание</a>
                                `]]
                                [[PageBuilder? &container=`product_tabs` &templates=`head`]]
                            </nav>

                            <div class="padded">
                                [[if? &is=`[*content*]~~!empty` &separator=`~~` &then=`
                                    <div id="description">
                                        <div class="user-content">
                                            [*content*]
                                        </div>
                                    </div>
                                `]]
                                [[PageBuilder? &container=`product_tabs`]]
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{footer}}
