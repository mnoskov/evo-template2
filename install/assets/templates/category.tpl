/**
 * category
 *
 * Список категорий и товаров
 *
 * @category template
 */
{{head}}
{{page_head[*head_type*]}}

<div class="page-layout">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="left-column">
                    <div class="hidden-sm-down">
                        {{left_menu}}
                    </div>

                    [!renderSideFilterForm!]
                </div>
            </div>

            <div class="col-xs-12 col-md-8">
                <div class="page-content">
                    {{page_default_head_image}}

                    <div class="full-width">
                        [[DocLister? 
                            &parents=`[*id*]` 
                            &tvList=`image`
                            &tpl=`categories_tpl3_item` 
                            &ownerTPL=`categories_tpl3_wrap` 
                            &orderBy=`c.menuindex ASC`
                            &addWhereList=`c.template = [*template*]`
                            &prepare=`prepareCategories`
                            &tplCode=`categories_tpl3`
                            &filterTpl=`[*template*]`
                        ]]
                    </div>

                    [!renderProducts!]

                    [[if? &is=`[*content*]~~!empty` &separator=`~~` &then=`
                        <div class="user-content">
                            [*content*]
                        </div>
                    `]]
                </div>
            </div>
        </div>
    </div>
</div>

{{footer}}
