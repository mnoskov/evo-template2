/**
 * reviews-list
 *
 * Список отзывов
 *
 * @category template
 */
{{head}}
{{page_head[*head_type*]}}

<div class="page-layout">
    <div class="container">
        <div class="row">
            <div class="col-md-4 hidden-sm-down">
                <div class="left-column">
                    {{left_menu}}
                </div>
            </div>

            <div class="col-xs-12 col-md-8">
                <div class="page-content">
                    {{page_default_head_image}}

                    <div class="user-content">
                        [*content*]
                    </div>

                    [[DocLister?
                        &parents=`[*id*]`
                        &tvList=`image`
                        &tpl=`reviews_item`
                        &ownerTPL=`reviews_wrap`
                        &orderBy=`menuindex ASC`
                        &display=`12`
                        &paginate=`pages`
                        &TplCurrentPage=`@CODE: <span class="page current">[+num+]</span>`
                        &TplWrapPaginate=`@CODE:<div class="pagination">[+wrap+]</div>`
                        &TplPrevP=`@CODE: `
                        &TplNextP=`@CODE: `
                    ]]

                    <div class="full-width">
                        <div class="service-footer">
                            <div class="blocks-container">
                                <div>
                                    <a href="#" class="btn btn-theme" data-toggle="modal" data-target="#review">Оставить отзыв</a>
                                </div>

                                <div class="description">
                                    Понравилась наша компания? Вы можете оставить отзыв, он будет опубликован после модерации.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{modal_review}}
{{footer}}
