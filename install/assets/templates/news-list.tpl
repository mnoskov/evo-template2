/**
 * news-list
 *
 * Список
 *
 * @category template
 */
{{head}}
{{page_head[*head_type*]}}

<div class="page-layout">
    <div class="container">
        <div class="row">
            <div class="col-md-4 hidden-sm-down">
                <div class="left-column">
                    {{left_menu_articles}}
                </div>
            </div>

            <div class="col-xs-12 col-md-8">
                <div class="page-content">
                    [[DocLister? 
                        &parents=`[*id*]`
                        &tvList=`image`
                        &tpl=`list_item` 
                        &ownerTPL=`list_wrap` 
                        &orderBy=`pub_date DESC`
                        &display=`12`
                        &paginate=`pages`
                        &TplCurrentPage=`@CODE: <span class="page current">[+num+]</span>` 
                        &TplWrapPaginate=`@CODE:<div class="pagination">[+wrap+]</div>`
                        &TplPrevP=`@CODE: `
                        &TplNextP=`@CODE: `
                    ]]

                    [[if? &is=`[*content*]~~!empty` &separator=`~~` &then=`
                        <div class="user-content">
                            [*content*]
                        </div>
                    `]]
                </div>
            </div>
        </div>
    </div>
</div>

{{footer}}
