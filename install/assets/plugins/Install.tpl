//<?php
/**
 * Install
 *
 * Template installer
 *
 * @category    plugin
 * @author      mnoskov
 * @internal    @events OnWebPageInit,OnManagerPageInit,OnPageNotFound
 * @internal    @installset base
*/

$modx->clearCache('full');

$templates = [];
$query = $modx->db->select('*', $modx->getFullTablename('site_templates'));

while ($row = $modx->db->getRow($query)) {
    $templates[$row['templatename']] = $row['id'];
}

$tvs = [];
$query = $modx->db->select('*', $modx->getFullTablename('site_tmplvars'));

while ($row = $modx->db->getRow($query)) {
    $tvs[$row['name']] = $row['id'];
}

$content_data = [
    [
        'id' => '6',
        'pagetitle' => 'Компания',
        'alias' => 'company',
        'published' => '1',
        'pub_date' => '1538550165',
        'parent' => '1',
        'isfolder' => '1',
        'template' => $templates['list'],
        'menuindex' => '1',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '7',
        'pagetitle' => 'Каталог',
        'alias' => 'catalog',
        'published' => '1',
        'pub_date' => '1538550206',
        'parent' => '1',
        'isfolder' => '1',
        'content' => '<p>Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс. Комета, следуя пионерской работе Эдвина Хаббла, трансформирует героический миф. Роджерс определял терапию как, природа гамма-всплексов гасит субъект.</p>
    <p>Беллетристика рассматривается невротический натуральный логарифм. Конфликт амбивалентно отражает непреложный азимут, Плутон не входит в эту классификацию. Отношение к современности традиционно дает Ганимед. Высота возможна. Опера-буффа образует апогей. Очевидно, что "кодекс деяний" методологически колеблет дедуктивный метод.</p>',
        'template' => $templates['category'],
        'menuindex' => '2',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '8',
        'pagetitle' => 'Услуги',
        'alias' => 'services',
        'published' => '1',
        'pub_date' => '1538550251',
        'parent' => '1',
        'isfolder' => '1',
        'template' => $templates['list'],
        'menuindex' => '3',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '9',
        'pagetitle' => 'Информация',
        'alias' => 'information',
        'published' => '1',
        'pub_date' => '1538550263',
        'parent' => '1',
        'isfolder' => '1',
        'template' => $templates['list'],
        'menuindex' => '4',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '10',
        'pagetitle' => 'Контакты',
        'alias' => 'contacts',
        'published' => '1',
        'pub_date' => '1538550284',
        'parent' => '1',
        'content' => '<p>Беллетристика рассматривается невротический натуральный логарифм. Конфликт амбивалентно отражает непреложный азимут, Плутон не входит в эту классификацию. Отношение к современности традиционно дает Ганимед. Высота возможна. Опера-буффа образует апогей. Очевидно, что "кодекс деяний" методологически колеблет дедуктивный метод.</p>
    <p>Как мы уже знаем, исчисление предикатов параллельно. Все известные астероиды имеют прямое движение, при этом элегия отчуждает конвергентный классицизм, но кольца видны только при 40&ndash;50. Конформность, согласно традиционным представлениям, преобразует онтологический предмет деятельности, что-то подобное можно встретить в работах Ауэрбаха и Тандлера.</p>',
        'template' => $templates['contacts'],
        'menuindex' => '5',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '11',
        'pagetitle' => 'О компании',
        'alias' => 'about',
        'published' => '1',
        'pub_date' => '1538550809',
        'parent' => '6',
        'content' => '<p>Азимут решает популяционный индекс. Многие кометы имеют два хвоста, однако жизнь индуктивно гасит поперечник. Можно предположить, что здравый смысл ясен не всем. Очевидно, что эклиптика ничтожно дает возмущающий фактор. Узел, по определению, решительно выводит непредвиденный афелий . Абстракция иллюзорна.</p>
    <p>Даосизм преобразует узел. Ощущение мира, после осторожного анализа, традиционно транспонирует зенит. Гегельянство, следуя пионерской работе Эдвина Хаббла, выслеживает межпланетный знак. Туманность Андромеды многопланово решает гений.</p>',
        'template' => $templates['info'],
        'menuindex' => '0',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '12',
        'pagetitle' => 'Сертификаты',
        'longtitle' => 'Лицензии и сертификаты',
        'alias' => 'certificates',
        'published' => '1',
        'pub_date' => '1538551982',
        'parent' => '6',
        'content' => '<p>Как мы уже знаем, исчисление предикатов параллельно. Все известные астероиды имеют прямое движение, при этом элегия отчуждает конвергентный классицизм, но кольца видны только при 40&ndash;50. Конформность, согласно традиционным представлениям, преобразует онтологический предмет деятельности, что-то подобное можно встретить в работах Ауэрбаха и Тандлера.</p>
    <p>Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс. Комета, следуя пионерской работе Эдвина Хаббла, трансформирует героический миф. Роджерс определял терапию как, природа гамма-всплексов гасит субъект.</p>',
        'template' => $templates['gallery'],
        'menuindex' => '1',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '13',
        'pagetitle' => 'Новости',
        'alias' => 'news',
        'published' => '1',
        'pub_date' => '1538554899',
        'parent' => '9',
        'isfolder' => '1',
        'template' => $templates['news-list'],
        'menuindex' => '0',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '14',
        'pagetitle' => 'Статьи',
        'alias' => 'articles',
        'published' => '1',
        'pub_date' => '1538554924',
        'parent' => '9',
        'isfolder' => '1',
        'template' => $templates['news-list'],
        'menuindex' => '1',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '15',
        'pagetitle' => 'Новость №1',
        'alias' => 'novost-n1',
        'published' => '1',
        'pub_date' => '1538555013',
        'parent' => '13',
        'introtext' => 'Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс. Комета, следуя пионерской работе Эдвина Хаббла, трансформирует героический миф. Роджерс определял терапию как, природа гамма-всплексов гасит субъект.',
        'content' => '<p>Беллетристика рассматривается невротический натуральный логарифм. Конфликт амбивалентно отражает непреложный азимут, Плутон не входит в эту классификацию. Отношение к современности традиционно дает Ганимед. Высота возможна. Опера-буффа образует апогей. Очевидно, что "кодекс деяний" методологически колеблет дедуктивный метод.</p>
    <p>Как мы уже знаем, исчисление предикатов параллельно. Все известные астероиды имеют прямое движение, при этом элегия отчуждает конвергентный классицизм, но кольца видны только при 40&ndash;50. Конформность, согласно традиционным представлениям, преобразует онтологический предмет деятельности, что-то подобное можно встретить в работах Ауэрбаха и Тандлера.</p>
    <p>Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс. Комета, следуя пионерской работе Эдвина Хаббла, трансформирует героический миф. Роджерс определял терапию как, природа гамма-всплексов гасит субъект.</p>',
        'template' => $templates['article'],
        'menuindex' => '0',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '16',
        'pagetitle' => 'Новость №2',
        'alias' => 'novost-n2',
        'published' => '1',
        'pub_date' => '1538741540',
        'parent' => '13',
        'introtext' => 'Как мы уже знаем, исчисление предикатов параллельно. Все известные астероиды имеют прямое движение, при этом элегия отчуждает конвергентный классицизм, но кольца видны только при 40–50.',
        'template' => $templates['article'],
        'menuindex' => '1',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '17',
        'pagetitle' => 'Новость №3',
        'alias' => 'novost-n3',
        'published' => '1',
        'pub_date' => '1538741540',
        'parent' => '13',
        'introtext' => 'Конформность, согласно традиционным представлениям, преобразует онтологический предмет деятельности, что-то подобное можно встретить в работах Ауэрбаха и Тандлера.',
        'template' => $templates['article'],
        'menuindex' => '2',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '18',
        'pagetitle' => 'Статья №1',
        'alias' => 'statya-n1',
        'published' => '1',
        'pub_date' => '1538648879',
        'parent' => '14',
        'introtext' => 'Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс. Комета, следуя пионерской работе Эдвина Хаббла, трансформирует героический миф. Роджерс определял терапию как, природа гамма-всплексов гасит субъект.',
        'template' => $templates['article'],
        'menuindex' => '0',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '19',
        'pagetitle' => 'Статья №2',
        'alias' => 'statya-n2',
        'published' => '1',
        'pub_date' => '1538648907',
        'parent' => '14',
        'introtext' => 'Как мы уже знаем, исчисление предикатов параллельно. Все известные астероиды имеют прямое движение, при этом элегия отчуждает конвергентный классицизм, но кольца видны только при 40–50. Конформность, согласно традиционным представлениям, преобразует онтологический предмет деятельности, что-то подобное можно встретить в работах Ауэрбаха и Тандлера.',
        'template' => $templates['article'],
        'menuindex' => '1',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '20',
        'pagetitle' => 'Категория услуг №1',
        'alias' => 'kategoriya-uslug-n1',
        'published' => '1',
        'pub_date' => '1538649131',
        'parent' => '8',
        'isfolder' => '1',
        'introtext' => 'Как мы уже знаем, исчисление предикатов параллельно. Все известные астероиды имеют прямое движение, при этом элегия отчуждает конвергентный классицизм, но кольца видны только при 40–50. Конформность, согласно традиционным представлениям, преобразует онтологический предмет деятельности, что-то подобное можно встретить в работах Ауэрбаха и Тандлера.',
        'template' => $templates['list'],
        'menuindex' => '0',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '21',
        'pagetitle' => 'Категория услуг №2',
        'alias' => 'kategoriya-uslug-n2',
        'published' => '1',
        'pub_date' => '1538649148',
        'parent' => '8',
        'template' => $templates['list'],
        'menuindex' => '1',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '22',
        'pagetitle' => 'Категория услуг №3',
        'alias' => 'kategoriya-uslug-n3',
        'published' => '1',
        'pub_date' => '1538649157',
        'parent' => '8',
        'template' => $templates['list'],
        'menuindex' => '2',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '23',
        'pagetitle' => 'Категория услуг №4',
        'alias' => 'kategoriya-uslug-n4',
        'published' => '1',
        'pub_date' => '1538649166',
        'parent' => '8',
        'template' => $templates['list'],
        'menuindex' => '3',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '24',
        'pagetitle' => 'Услуга №1',
        'alias' => 'usluga-n1',
        'published' => '1',
        'pub_date' => '1538649433',
        'parent' => '20',
        'introtext' => 'Беллетристика рассматривается невротический натуральный логарифм. Конфликт амбивалентно отражает непреложный азимут, Плутон не входит в эту классификацию. Отношение к современности традиционно дает Ганимед. Высота возможна.',
        'content' => '<p>Беллетристика рассматривается невротический натуральный логарифм. Конфликт амбивалентно отражает непреложный азимут, Плутон не входит в эту классификацию. Отношение к современности традиционно дает Ганимед. Высота возможна. Опера-буффа образует апогей. Очевидно, что "кодекс деяний" методологически колеблет дедуктивный метод.</p>
    <p>Как мы уже знаем, исчисление предикатов параллельно. Все известные астероиды имеют прямое движение, при этом элегия отчуждает конвергентный классицизм, но кольца видны только при 40&ndash;50. Конформность, согласно традиционным представлениям, преобразует онтологический предмет деятельности, что-то подобное можно встретить в работах Ауэрбаха и Тандлера.</p>
    <p>Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс. Комета, следуя пионерской работе Эдвина Хаббла, трансформирует героический миф. Роджерс определял терапию как, природа гамма-всплексов гасит субъект.</p>',
        'template' => $templates['service'],
        'menuindex' => '0',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '25',
        'pagetitle' => 'Услуга №2',
        'alias' => 'usluga-n2',
        'published' => '1',
        'pub_date' => '1538649618',
        'parent' => '20',
        'template' => $templates['service'],
        'menuindex' => '1',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '26',
        'pagetitle' => 'Услуга №3',
        'alias' => 'usluga-n3',
        'published' => '1',
        'pub_date' => '1538649675',
        'parent' => '20',
        'template' => $templates['service'],
        'menuindex' => '2',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '27',
        'pagetitle' => 'Услуга №4',
        'alias' => 'usluga-n4',
        'published' => '1',
        'pub_date' => '1538649694',
        'parent' => '20',
        'template' => $templates['service'],
        'menuindex' => '3',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '28',
        'pagetitle' => 'Услуга №5',
        'alias' => 'usluga-n5',
        'published' => '1',
        'pub_date' => '1538649710',
        'parent' => '20',
        'template' => $templates['service'],
        'menuindex' => '4',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '29',
        'pagetitle' => 'Услуга №6',
        'alias' => 'usluga-n6',
        'published' => '1',
        'pub_date' => '1538649737',
        'parent' => '20',
        'template' => $templates['service'],
        'menuindex' => '5',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '30',
        'pagetitle' => 'Телевизоры',
        'alias' => 'tv',
        'published' => '1',
        'pub_date' => '1538650797',
        'parent' => '7',
        'isfolder' => '1',
        'introtext' => 'Конфликт амбивалентно отражает непреложный азимут',
        'template' => $templates['category'],
        'menuindex' => '0',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '31',
        'pagetitle' => 'Видеоплееры',
        'alias' => 'video',
        'published' => '1',
        'pub_date' => '1538650814',
        'parent' => '7',
        'isfolder' => '1',
        'introtext' => 'Очевидно, что "кодекс деяний" методологически колеблет дедуктивный метод',
        'template' => $templates['category'],
        'menuindex' => '1',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '32',
        'pagetitle' => 'Аудио и Hi-Fi техника',
        'alias' => 'audio',
        'published' => '1',
        'pub_date' => '1538650840',
        'parent' => '7',
        'introtext' => 'Высота возможна. Опера-буффа образует апогей',
        'template' => $templates['category'],
        'menuindex' => '2',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '33',
        'pagetitle' => 'Аксессуары для телевизоров, аудио, видео',
        'alias' => 'aksessuary-dlya-televizorov-audio-video',
        'published' => '1',
        'pub_date' => '1538650868',
        'parent' => '7',
        'introtext' => 'Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс',
        'template' => $templates['category'],
        'menuindex' => '3',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '34',
        'pagetitle' => 'Компьютеры',
        'alias' => 'kompyutery',
        'published' => '1',
        'pub_date' => '1538650886',
        'parent' => '7',
        'introtext' => 'Комета, следуя пионерской работе Эдвина Хаббла, трансформирует героический миф',
        'template' => $templates['category'],
        'menuindex' => '4',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '35',
        'pagetitle' => 'Smart TV Телевизоры',
        'alias' => 'smart-tv',
        'published' => '1',
        'pub_date' => '1538650954',
        'parent' => '30',
        'isfolder' => '1',
        'introtext' => 'Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс',
        'content' => '<p>Как мы уже знаем, исчисление предикатов параллельно. Все известные астероиды имеют прямое движение, при этом элегия отчуждает конвергентный классицизм, но кольца видны только при 40&ndash;50.</p>
    <p>Конформность, согласно традиционным представлениям, преобразует онтологический предмет деятельности, что-то подобное можно встретить в работах Ауэрбаха и Тандлера.</p>',
        'template' => $templates['category'],
        'menuindex' => '0',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '36',
        'pagetitle' => 'OLED телевизоры',
        'alias' => 'oled',
        'published' => '1',
        'pub_date' => '1538651002',
        'parent' => '30',
        'template' => $templates['category'],
        'menuindex' => '1',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '37',
        'pagetitle' => 'Full HD телевизоры',
        'alias' => 'full-hd',
        'published' => '1',
        'pub_date' => '1538651020',
        'parent' => '30',
        'template' => $templates['category'],
        'menuindex' => '2',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '38',
        'pagetitle' => 'Медиаплееры',
        'alias' => 'mediapleery',
        'published' => '1',
        'pub_date' => '1538651071',
        'parent' => '31',
        'template' => $templates['category'],
        'menuindex' => '0',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '39',
        'pagetitle' => 'DVD плееры',
        'alias' => 'dvd-pleery',
        'published' => '1',
        'pub_date' => '1538651079',
        'parent' => '31',
        'template' => $templates['category'],
        'menuindex' => '1',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '40',
        'pagetitle' => ' Samsung UE55MU6300',
        'alias' => 'samsung-ue55mu6300',
        'published' => '1',
        'pub_date' => '1538651717',
        'parent' => '35',
        'template' => $templates['product'],
        'menuindex' => '1',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '41',
        'pagetitle' => 'Akai LES-32Z73T',
        'alias' => 'akai-les-32z73t',
        'published' => '1',
        'pub_date' => '1538651903',
        'parent' => '35',
        'introtext' => 'Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс. Комета, следуя пионерской работе Эдвина Хаббла, трансформирует героический миф. Роджерс определял терапию как, природа гамма-всплексов гасит субъект.',
        'content' => '<p>Беллетристика рассматривается невротический натуральный логарифм. Конфликт амбивалентно отражает непреложный азимут, Плутон не входит в эту классификацию. Отношение к современности традиционно дает Ганимед. Высота возможна. Опера-буффа образует апогей. Очевидно, что "кодекс деяний" методологически колеблет дедуктивный метод.</p>
    <p>Как мы уже знаем, исчисление предикатов параллельно. Все известные астероиды имеют прямое движение, при этом элегия отчуждает конвергентный классицизм, но кольца видны только при 40&ndash;50. Конформность, согласно традиционным представлениям, преобразует онтологический предмет деятельности, что-то подобное можно встретить в работах Ауэрбаха и Тандлера.</p>
    <p>Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс. Комета, следуя пионерской работе Эдвина Хаббла, трансформирует героический миф. Роджерс определял терапию как, природа гамма-всплексов гасит субъект.</p>',
        'template' => $templates['product'],
        'menuindex' => '0',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '42',
        'pagetitle' => 'LG 43UJ670V',
        'alias' => 'lg-43uj670v',
        'published' => '1',
        'pub_date' => '1538651924',
        'parent' => '35',
        'template' => $templates['product'],
        'menuindex' => '2',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '43',
        'pagetitle' => 'LG 43UJ634V',
        'alias' => 'lg-43uj634v',
        'published' => '1',
        'pub_date' => '1538655234',
        'parent' => '35',
        'template' => $templates['product'],
        'menuindex' => '3',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '44',
        'pagetitle' => 'Supra STV-LC50ST1001F',
        'alias' => 'supra-stv-lc50st1001f',
        'published' => '1',
        'pub_date' => '1538655289',
        'parent' => '35',
        'template' => $templates['product'],
        'menuindex' => '5',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '45',
        'pagetitle' => 'LG 49LJ594V',
        'alias' => 'lg-49lj594v',
        'published' => '1',
        'pub_date' => '1538655363',
        'parent' => '35',
        'template' => $templates['product'],
        'menuindex' => '4',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '46',
        'pagetitle' => 'QLED телевизоры',
        'alias' => 'qled',
        'published' => '1',
        'pub_date' => '1538721212',
        'parent' => '30',
        'template' => $templates['category'],
        'menuindex' => '3',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '47',
        'pagetitle' => 'Nano Cell телевизоры',
        'alias' => 'nano-cell',
        'published' => '1',
        'pub_date' => '1538721267',
        'parent' => '30',
        'template' => $templates['category'],
        'menuindex' => '4',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '48',
        'pagetitle' => 'Ultra HD (4k) телевизоры',
        'alias' => 'ultra-hd',
        'published' => '1',
        'pub_date' => '1538721291',
        'parent' => '30',
        'template' => $templates['category'],
        'menuindex' => '5',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '49',
        'pagetitle' => 'Android TV',
        'alias' => 'android-tv',
        'published' => '1',
        'pub_date' => '1538721314',
        'parent' => '30',
        'template' => $templates['category'],
        'menuindex' => '6',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '50',
        'pagetitle' => '3D телевизоры',
        'alias' => '3d',
        'published' => '1',
        'pub_date' => '1538721332',
        'parent' => '30',
        'template' => $templates['category'],
        'menuindex' => '7',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '51',
        'pagetitle' => 'Результаты поиска',
        'alias' => 'search',
        'published' => '1',
        'pub_date' => '1538741540',
        'parent' => '0',
        'template' => $templates['search'],
        'menuindex' => '4',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '52',
        'pagetitle' => 'Сотрудники',
        'alias' => 'sotrudniki',
        'published' => '1',
        'pub_date' => '1538741540',
        'parent' => '6',
        'template' => $templates['employees'],
        'menuindex' => '2',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '53',
        'pagetitle' => 'Отзывы',
        'alias' => 'otzyvy',
        'pub_date' => '1539597666',
        'published' => '1',
        'parent' => '6',
        'isfolder' => '1',
        'content' => '<p>Мы стремимся оказывать качественные услуги с высоким уровнем сервиса. Мы благодарны&nbsp;нашим клиентам за оказанное доверие и положительные отзывы о совместной работе. Если хотите, можем помочь и вам: наладим работу отдела продаж, установим системы видеонаблюдения и автоматизации, разработаем дизайн интерьера под любой стиль и бюджет. Просто свяжитесь с нами!</p>',
        'template' => $templates['reviews-list'],
        'menuindex' => '3',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '54',
        'pagetitle' => 'Елена Шипрова',
        'alias' => 'elena-shiprova',
        'pub_date' => '1539597698',
        'published' => '1',
        'parent' => '53',
        'introtext' => 'директор офисного центра Getred',
        'content' => '<p>Беллетристика рассматривается невротический натуральный логарифм. Конфликт амбивалентно отражает непреложный азимут, Плутон не входит в эту классификацию. Отношение к современности традиционно дает Ганимед. Высота возможна. Опера-буффа образует апогей. Очевидно, что "кодекс деяний" методологически колеблет дедуктивный метод.</p><p>Как мы уже знаем, исчисление предикатов параллельно. Все известные астероиды имеют прямое движение, при этом элегия отчуждает конвергентный классицизм, но кольца видны только при 40&ndash;50. Конформность, согласно традиционным представлениям, преобразует онтологический предмет деятельности, что-то подобное можно встретить в работах Ауэрбаха и Тандлера.</p><p>Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс. Комета, следуя пионерской работе Эдвина Хаббла, трансформирует героический миф. Роджерс определял терапию как, природа гамма-всплексов гасит субъект.</p>',
        'richtext' => '1',
        'template' => $templates['review'],
        'menuindex' => '0',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '55',
        'pagetitle' => 'Алина Миронова',
        'alias' => 'alina-mironova',
        'pub_date' => '1538741540',
        'published' => '1',
        'parent' => '53',
        'introtext' => 'директор офисного центра Getred',
        'content' => '<p>Беллетристика рассматривается невротический натуральный логарифм. Конфликт амбивалентно отражает непреложный азимут, Плутон не входит в эту классификацию. Отношение к современности традиционно дает Ганимед. Высота возможна. Опера-буффа образует апогей. Очевидно, что "кодекс деяний" методологически колеблет дедуктивный метод.</p><p>Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс. Комета, следуя пионерской работе Эдвина Хаббла, трансформирует героический миф. Роджерс определял терапию как, природа гамма-всплексов гасит субъект.</p>',
        'template' => $templates['review'],
        'menuindex' => '1',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ], [
        'id' => '56',
        'pagetitle' => 'Павел Дергунин',
        'alias' => 'pavel-dergunin',
        'pub_date' => '1538741540',
        'published' => '1',
        'parent' => '53',
        'introtext' => 'директор офисного центра Getred',
        'content' => '<p>Как мы уже знаем, исчисление предикатов параллельно. Все известные астероиды имеют прямое движение, при этом элегия отчуждает конвергентный классицизм, но кольца видны только при 40&ndash;50. Конформность, согласно традиционным представлениям, преобразует онтологический предмет деятельности, что-то подобное можно встретить в работах Ауэрбаха и Тандлера.</p><p>Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс. Комета, следуя пионерской работе Эдвина Хаббла, трансформирует героический миф. Роджерс определял терапию как, природа гамма-всплексов гасит субъект.</p>',
        'template' => $templates['review'],
        'menuindex' => '2',
        'createdby' => '1',
        'editedby' => '1',
        'publishedby' => '1',
    ],
];

$site_content = $modx->getFullTablename('site_content');

foreach ($content_data as $row) {
    foreach (['pagetitle', 'longtitle', 'content', 'introtext'] as $field) {
        if (isset($row[$field])) {
            $row[$field] = $modx->db->escape($row[$field]);
        }
    }

    $modx->db->insert($row, $site_content);
}

$site_tmplvar_templates = $modx->getFullTablename('site_tmplvar_templates');
$all_templates = array_keys($templates);
$all_templates_but_main = array_diff($all_templates, ['main']);

$tmplvar_templates_date = [
    'meta_description' => $all_templates,
    'meta_keywords'    => $all_templates,
    'meta_title'       => $all_templates,
    'og_description'   => $all_templates,
    'og_image'         => $all_templates,
    'og_title'         => $all_templates,
    'head_background'  => $all_templates_but_main,
    'head_image'       => $all_templates_but_main,
    'head_type'        => $all_templates_but_main,
    'image'            => $all_templates_but_main,
    'gallery_format'   => ['gallery'],
    'map_code'         => ['contacts'],
    'show_on_main'     => ['list', 'category', 'service'],
    'tovarparams'      => ['category', 'search'],
    'labels'           => ['product'],
    'price'            => ['product'],
    'old_price'        => ['product'],
    'best'             => ['product'],
    'color'            => ['product'],
    'dimension'        => ['product'],
    'hd_type'          => ['product'],
    'size'             => ['product'],
];

foreach ($tmplvar_templates_date as $tvname => $tnames) {
    $modx->db->delete($site_tmplvar_templates, "`tmplvarid` = '" . $tvs[$tvname] . "'");

    foreach ($tnames as $tname) {
        $modx->db->insert([
            'tmplvarid'  => $tvs[$tvname],
            'templateid' => $templates[$tname],
        ], $site_tmplvar_templates);
    }
}

$tmplvar_contentvalues_data = [
    '7' => [
        'tovarparams' => $modx->db->escape(json_encode([
            'fieldValue' => [
                [
                    'param_id' => $tvs['price'],
                    'cat_name' => '',
                    'list_yes' => '',
                    'fltr_yes' => '1',
                    'fltr_type' => '6',
                    'fltr_name' => 'Цена, руб.',
                    'fltr_many' => '',
                    'fltr_href' => '',
                ], [
                    'param_id' => $tvs['size'],
                    'cat_name' => '',
                    'list_yes' => '',
                    'fltr_yes' => '1',
                    'fltr_type' => '6',
                    'fltr_name' => 'Диагональ телевизора',
                    'fltr_many' => '',
                    'fltr_href' => '',
                ], [
                    'param_id' => $tvs['hd_type'],
                    'cat_name' => '',
                    'list_yes' => '',
                    'fltr_yes' => '1',
                    'fltr_type' => '1',
                    'fltr_name' => 'Разрешение',
                    'fltr_many' => '',
                    'fltr_href' => '',
                ], [
                    'param_id' => $tvs['dimension'],
                    'cat_name' => '',
                    'list_yes' => '',
                    'fltr_yes' => '1',
                    'fltr_type' => '1',
                    'fltr_name' => 'Разрешение экрана',
                    'fltr_many' => '',
                    'fltr_href' => '',
                ], [
                    'param_id' => $tvs['color'],
                    'cat_name' => '',
                    'list_yes' => '',
                    'fltr_yes' => '1',
                    'fltr_type' => '1',
                    'fltr_name' => 'Цвет',
                    'fltr_many' => '',
                    'fltr_href' => '',
                ]
            ],
            'fieldSettings' => [
                'autoincrement' => 1
            ]
        ], JSON_UNESCAPED_UNICODE)),
    ],
    '10' => [
        'map_code' => $modx->db->escape('<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A_osAArkT6fVEofrqpHPwBGoDygiV5x-3&amp;width=100%25&amp;height=480&amp;lang=ru_RU&amp;scroll=true"></script>'),
    ],
    '11' => [
        'head_image' => 'assets/images/demo/example2.jpg',
    ],
    '12' => [
        'gallery_format' => '1',
    ],
    '15' => [
        'image' => 'assets/images/demo/example3.jpg',
        'head_image' => 'assets/images/demo/example5.jpg',
    ],
    '16' => [
        'image' => 'assets/images/demo/example6.jpg',
    ],
    '17' => [
        'image' => 'assets/images/demo/example8.jpg',
    ],
    '18' => [
        'image' => 'assets/images/demo/example2.jpg',
    ],
    '19' => [
        'image' => 'assets/images/demo/example3.jpg',
    ],
    '20' => [
        'image' => 'assets/images/demo/example2.jpg',
    ],
    '21' => [
        'image' => 'assets/images/demo/example7.jpg',
    ],
    '22' => [
        'image' => 'assets/images/demo/example9.jpg',
    ],
    '23' => [
        'image' => 'assets/images/demo/example1.jpg',
    ],
    '24' => [
        'image' => 'assets/images/demo/example8.jpg',
        'show_on_main' => '1',
        'head_image' => 'assets/images/demo/example8.jpg',
    ],
    '25' => [
        'image' => 'assets/images/demo/example7.jpg',
    ],
    '26' => [
        'image' => 'assets/images/demo/example1.jpg',
        'show_on_main' => '1',
    ],
    '27' => [
        'image' => 'assets/images/demo/example6.jpg',
    ],
    '28' => [
        'image' => 'assets/images/demo/example4.jpg',
        'show_on_main' => '1',
    ],
    '29' => [
        'image' => 'assets/images/demo/example2.jpg',
    ],
    '30' => [
        'image' => 'assets/images/demo/example1.jpg',
        'show_on_main' => '1',
    ],
    '31' => [
        'image' => 'assets/images/demo/example2.jpg',
        'show_on_main' => '1',
    ],
    '32' => [
        'image' => 'assets/images/demo/example3.jpg',
    ],
    '33' => [
        'image' => 'assets/images/demo/example4.jpg',
        'show_on_main' => '1',
    ],
    '34' => [
        'image' => 'assets/images/demo/example5.jpg',
    ],
    '35' => [
        'image' => 'assets/images/demo/example6.jpg',
        'show_on_main' => '1',
        'head_type' => '_extended',
        'head_background' => 'assets/images/demo/slide1-background.jpg',
        'head_image' => 'assets/images/demo/slide1-foreground.png',
    ],
    '36' => [
        'image' => 'assets/images/demo/example7.jpg',
    ],
    '37' => [
        'image' => 'assets/images/demo/example8.jpg',
    ],
    '38' => [
        'image' => 'assets/images/demo/example9.jpg',
    ],
    '39' => [
        'image' => 'assets/images/demo/example3.jpg',
    ],
    '40' => [
        'price'  => '19900',
        'best'  => '1',
        'image' => 'assets/images/demo/example1.jpg',
        'size' => '50',
        'labels' => '3',
        'old_price' => '23200',
        'dimension' => '3840x2160',
        'hd_type' => '4K Ultra HD',
        'color' => 'черный, серебристый',
    ],
    '41' => [
        'price'  => '15000',
        'best'  => '1',
        'image' => 'assets/images/demo/example2.jpg',
        'size' => '31',
        'labels' => '3||4',
        'old_price' => '16750',
        'head_type' => '_extended',
        'head_background' => 'assets/images/demo/slide2-background.jpg',
        'head_image' => 'assets/images/demo/slide2-foreground.png',
        'dimension' => '1366x768',
        'hd_type' => '720p',
        'color' => 'черный',
    ],
    '42' => [
        'price'  => '36000',
        'best'  => '1',
        'image' => 'assets/images/demo/example3.jpg',
        'size' => '37',
        'dimension' => '3840x2160',
        'hd_type' => '4K Ultra HD',
        'color' => 'серебристый',
    ],
    '43' => [
        'price'  => '29990',
        'best'  => '1',
        'image' => 'assets/images/demo/example4.jpg',
        'size' => '43',
        'labels' => '1',
        'dimension' => '3840x2160',
        'hd_type' => '4K Ultra HD',
        'color' => 'черный',
    ],
    '44' => [
        'price'  => '30690',
        'best'  => '1',
        'image' => 'assets/images/demo/example7.jpg',
        'size' => '50',
        'labels' => '3',
        'old_price' => '32300',
        'dimension' => '1920x1080',
        'hd_type' => '1080p',
        'color' => 'серебристый',
    ],
    '45' => [
        'price'  => '31990',
        'best'  => '1',
        'image' => 'assets/images/demo/example5.jpg',
        'size' => '49',
        'dimension' => '1920x1080',
        'hd_type' => '1080p',
        'color' => 'черный',
    ],
    '46' => [
        'image' => 'assets/images/demo/example1.jpg',
    ],
    '47' => [
        'image' => 'assets/images/demo/example4.jpg',
    ],
    '48' => [
        'image' => 'assets/images/demo/example2.jpg',
    ],
    '49' => [
        'image' => 'assets/images/demo/example3.jpg',
    ],
    '50' => [
        'image' => 'assets/images/demo/example9.jpg',
    ],
    '54' => [
        'image' => 'assets/images/demo/example2.jpg',
    ],
    '55' => [
        'image' => 'assets/images/demo/example2.jpg',
    ],
    '56' => [
        'image' => 'assets/images/demo/example2.jpg',
    ],
];

$site_tmplvar_contentvalues = $modx->getFullTablename('site_tmplvar_contentvalues');

foreach ($tmplvar_contentvalues_data as $docid => $values) {
    foreach ($values as $tvname => $value) {
        $modx->db->insert([
            'tmplvarid' => $tvs[$tvname],
            'contentid' => $docid,
            'value'     => $value,
        ], $site_tmplvar_contentvalues);
    }
}

$pagebuilder_data = [
    [
        'document_id' => '2',
        'container' => 'main',
        'config' => 'main_features',
        'values' => '{"items":[{"title":"Комплексный подход к выполнению услуг","image":"assets/images/demo/example5.jpg","text":"Высота возможна. Опера-буффа образует апогей"},{"title":"Доступные цены на все виды работ","image":"assets/images/demo/example2.jpg","text":"Комета, следуя пионерской работе Эдвина Хаббла, трансформирует героический миф"},{"title":"Оперативность оказания услуг","image":"assets/images/demo/example9.jpg","text":"Аномальная джетовая активность непредвзято вызывает межпланетный эдипов комплекс"}]}',
        'index' => '0',
    ], [
        'document_id' => '2',
        'container' => 'main',
        'config' => 'main_categories',
        'values' => '{"title":"Категории","source":"7","row_template":"categories_tpl3","template":"' . $templates['category'] . '"}',
        'index' => '1',
    ], [
        'document_id' => '2',
        'container' => 'main',
        'config' => 'main_partners',
        'values' => '{"items":[{"title":"","image":"assets/images/demo/example1.jpg"},{"title":"","image":"assets/images/demo/example2.jpg"},{"title":"","image":"assets/images/demo/example3.jpg"},{"title":"","image":"assets/images/demo/example4.jpg"},{"title":"","image":"assets/images/demo/example5.jpg"},{"title":"","image":"assets/images/demo/example6.jpg"},{"title":"","image":"assets/images/demo/example7.jpg"}]}',
        'index' => '2',
    ], [
        'document_id' => '2',
        'container' => 'main',
        'config' => 'main_best',
        'values' => '{"title":"Хиты продаж","btn":"Все товары","source":"7"}',
        'index' => '3',
    ], [
        'document_id' => '2',
        'container' => 'main',
        'config' => 'main_services',
        'values' => '{"title":"Наши услуги","btn_label":"Все услуги","source":"8","template":"' . $templates['service'] . '","text":"<p>Компания \\"[(site_name)]\\"&nbsp;&mdash; это команда квалифицированных специалистов, которые способны предоставить обширный спектр услуг каждый в своей, одному ему подвластной, области услуг.</p>"}',
        'index' => '4',
    ], [
        'document_id' => '2',
        'container' => 'main',
        'config' => 'main_articles',
        'values' => '{"title":"Статьи","btn_label":"Все статьи","source":"9","template":"' . $templates['article'] . '"}',
        'index' => '5',
    ], [
        'document_id' => '2',
        'container' => 'main',
        'config' => 'main_numbers',
        'values' => '{"title":"О компании","image":"assets/images/demo/example4.jpg","content":"<p>Компания \\"[(site_name)]\\" всегда на коне!</p>\\n<p>Выполняем работы любой сложности: помощь в получении, отчуждении, строительстве и ведении, сопровождение и поддержка, разработка и подготовка, а также проектирование, конструирование, моделирование и многое другое. В нашем штате есть специалисты с опытом более 60 лет&nbsp;в своей специальности и они все еще могут дать жару.</p>","numbers":[{"number":"1500","text":"Довольных клиентов"},{"number":"1503","text":"Разработанных проекта"},{"number":"2","text":"Года работы без происшествий"}]}',
        'index' => '6',
    ], [
        'document_id' => '2',
        'container' => 'main',
        'config' => 'main_reviews',
        'values' => '{"title":"Отзывы","btn_label":"Все отзывы"}',
        'index' => '7',
    ], [
        'document_id' => '2',
        'container' => 'main',
        'config' => 'main_instagram',
        'values' => '{"title":"Мы в Instagram","login":"pioneer.sk","token":"4860612270.1677ed0.7a9d44feb7b94c7bb8a16ca70529dc35"}',
        'index' => '8',
    ], [
        'document_id' => '2',
        'container' => 'main_cycle',
        'config' => 'main_cycle_slide',
        'values' => '{"title":"Геодезия от профессионалов ","background":"assets/images/demo/slide1-background.jpg","image":"assets/images/demo/slide1-foreground.png","text":"<p>На этом шаблоне уже запущен сайт<br /><a href=\\"https://гео-сфера.рф/\\" target=\\"_blank\\" rel=\\"noopener\\">гео-сфера.рф</a></p>","link":"https://гео-сфера.рф/","invert":{}}',
        'index' => '0',
    ], [
        'document_id' => '2',
        'container' => 'main_cycle',
        'config' => 'main_cycle_slide',
        'values' => '{"title":"Персональные тренировки","background":"assets/images/demo/slide2-background.jpg","image":"assets/images/demo/slide2-foreground.png","text":"<p>Макимальная эффективность за короткое время.<br />На этом шабоне запущен сайт <a href=\\"http://box.lmpg.ru/\\" target=\\"_blank\\" rel=\\"noopener\\">box.lmpg.ru</a></p>","link":"http://box.lmpg.ru/","invert":{"0":"1"}}',
        'index' => '1',
    ], [
        'document_id' => '52',
        'container' => 'employees',
        'config' => 'employees_division',
        'values' => '{"title":"Руководство","employees":[{"name":"Павел Дергунин","role":"Руководитель","image":"assets/images/demo/example2.jpg","description":"Контролирует деятельность организации и обеспечивает взаимодействие подразделений","content":"<p>Контролирует деятельность организации и обеспечивает взаимодействие подразделений Павел &ndash;&nbsp;наш бессменный руководитель, которому мы обязаны таким стремительным развитием компании.</p>\\n<p>На протяжении всех 9 лет он заботился о комфорте сотрудников и не забывал ответственно относится к нашим проектам, что делает его не просто руководителем, а наставником и помощником.</p>","phone":"","email":""},{"name":"Алина Миронова","role":"Основатель и художественный руководитель","image":"assets/images/demo/example3.jpg","description":"Разрабатывает стратегии продвижения по России и за рубежом","content":"","phone":"","email":""},{"name":"Попов Андрей Викторович","role":"Коммерческий директор","image":"assets/images/demo/example4.jpg","description":"Руководит финансово-экономической деятельностью предприятия","content":"","phone":"","email":""}]}',
        'index' => '0',
    ], [
        'document_id' => '52',
        'container' => 'employees',
        'config' => 'employees_division',
        'values' => '{"title":"Преподаватели","employees":[{"name":"Попов Андрей Викторович","role":"Преподаватель","image":"assets/images/demo/example5.jpg","description":"Помогает клиентам со сложными ситуациями на объекте и оформлением документации","content":"","phone":"","email":""},{"name":"Попов Андрей Викторович","role":"Преподаватель","image":"assets/images/demo/example9.jpg","description":"Помогает клиентам со сложными ситуациями на объекте и оформлением документации","content":"","phone":"","email":""},{"name":"Петр Малиновский","role":"Преподаватель","image":"assets/images/demo/example3.jpg","description":"Помогает клиентам со сложными ситуациями на объекте и оформлением документации","content":"","phone":"","email":""}]}',
        'index' => '1',
    ],
];

$pagebuilder = $modx->getFullTablename('pagebuilder');

$modx->db->delete($pagebuilder, "`document_id` IN (2, 52)");

foreach ($pagebuilder_data as $row) {
    $row['values'] = $modx->db->escape($row['values']);
    $modx->db->insert($row, $pagebuilder);
}

$sg_data = [
    [
        'sg_id' => '1',
        'sg_image' => 'assets/galleries/12/cert.jpg',
        'sg_title' => 'cert',
        'sg_properties' => '{"width":645,"height":912,"size":112012}',
        'sg_rid' => '12',
        'sg_index' => '0',
        'sg_createdon' => '2018-10-03 10:34:36',
    ], [
        'sg_id' => '2',
        'sg_image' => 'assets/galleries/12/cert1.jpg',
        'sg_title' => 'cert1',
        'sg_properties' => '{"width":595,"height":840,"size":226671}',
        'sg_rid' => '12',
        'sg_index' => '1',
        'sg_createdon' => '2018-10-03 10:34:37',
    ], [
        'sg_id' => '3',
        'sg_image' => 'assets/galleries/12/cert2.jpg',
        'sg_title' => 'cert2',
        'sg_properties' => '{"width":595,"height":840,"size":243432}',
        'sg_rid' => '12',
        'sg_index' => '2',
        'sg_createdon' => '2018-10-03 10:34:38',
    ], [
        'sg_id' => '4',
        'sg_image' => 'assets/galleries/41/example1.jpg',
        'sg_title' => 'example1',
        'sg_properties' => '{"width":1600,"height":1000,"size":341997}',
        'sg_rid' => '41',
        'sg_index' => '0',
        'sg_createdon' => '2018-10-05 12:57:50',
    ], [
        'sg_id' => '5',
        'sg_image' => 'assets/galleries/41/example2.jpg',
        'sg_title' => 'example2',
        'sg_properties' => '{"width":1600,"height":1000,"size":176028}',
        'sg_rid' => '41',
        'sg_index' => '1',
        'sg_createdon' => '2018-10-05 12:57:51',
    ], [
        'sg_id' => '6',
        'sg_image' => 'assets/galleries/41/example3.jpg',
        'sg_title' => 'example3',
        'sg_properties' => '{"width":1600,"height":1000,"size":385610}',
        'sg_rid' => '41',
        'sg_index' => '2',
        'sg_createdon' => '2018-10-05 12:57:53',
    ], [
        'sg_id' => '7',
        'sg_image' => 'assets/galleries/41/example4.jpg',
        'sg_title' => 'example4',
        'sg_properties' => '{"width":1440,"height":900,"size":288035}',
        'sg_rid' => '41',
        'sg_index' => '3',
        'sg_createdon' => '2018-10-05 12:57:55',
    ], [
        'sg_id' => '8',
        'sg_image' => 'assets/galleries/41/example5.jpg',
        'sg_title' => 'example5',
        'sg_properties' => '{"width":1680,"height":1050,"size":470004}',
        'sg_rid' => '41',
        'sg_index' => '4',
        'sg_createdon' => '2018-10-05 12:57:57',
    ], [
        'sg_id' => '9',
        'sg_image' => 'assets/galleries/41/example6.jpg',
        'sg_title' => 'example6',
        'sg_properties' => '{"width":1600,"height":1200,"size":610881}',
        'sg_rid' => '41',
        'sg_index' => '5',
        'sg_createdon' => '2018-10-05 12:57:58',
    ], [
        'sg_id' => '10',
        'sg_image' => 'assets/galleries/41/example7.jpg',
        'sg_title' => 'example7',
        'sg_properties' => '{"width":1600,"height":1200,"size":573092}',
        'sg_rid' => '41',
        'sg_index' => '6',
        'sg_createdon' => '2018-10-05 12:58:00',
    ], [
        'sg_id' => '11',
        'sg_image' => 'assets/galleries/41/example8.jpg',
        'sg_title' => 'example8',
        'sg_properties' => '{"width":1600,"height":1200,"size":664743}',
        'sg_rid' => '41',
        'sg_index' => '7',
        'sg_createdon' => '2018-10-05 12:58:02',
    ], [
        'sg_id' => '12',
        'sg_image' => 'assets/galleries/41/example9.jpg',
        'sg_title' => 'example9',
        'sg_properties' => '{"width":1919,"height":1408,"size":503107}',
        'sg_rid' => '41',
        'sg_index' => '8',
        'sg_createdon' => '2018-10-05 12:58:05',
    ],
];

include_once(MODX_BASE_PATH . 'assets/plugins/simplegallery/lib/plugin.class.php');
global $modx_lang_attribute;
$plugin = new \SimpleGallery\sgPlugin($modx, $modx_lang_attribute);
$plugin->createTable();

$sg_images = $modx->getFullTablename('sg_images');

$modx->db->delete($sg_images);

foreach ($sg_data as $row) {
    $modx->db->insert($row, $sg_images);
}

$site_plugins = $modx->getFullTablename('site_plugins');
$site_plugin_events = $modx->getFullTablename('site_plugin_events');

$modx->db->update(['disabled' => 0], $site_plugins, "`name` = 'evoSearch'");

$plugin = $modx->db->getRow($modx->db->select('*', $site_plugins, "`name` = 'SimpleGallery'"));

if ($plugin) {
    $properties = json_decode($plugin['properties'], true);

    if ($properties) {
        if (!empty($properties['tabName'][0])) {
            $properties['tabName'][0]['value'] = 'Изображения';
        }

        if (!empty($properties['templates'][0])) {
            $properties['templates'][0]['value'] = $templates['product'] . ',' . $templates['gallery'];
        }

        $modx->db->update(['properties' => json_encode($properties, JSON_UNESCAPED_UNICODE)], $site_plugins, "`id` = '" . $plugin['id'] . "'");
    }
}

$plugin = $modx->db->getRow($modx->db->select('*', $site_plugins, "`name` = 'LessCompiler'"));

if ($plugin) {
    $properties = json_decode($plugin['properties'], true);

    if ($properties) {
        if (!empty($properties['path'][0])) {
            $properties['path'][0]['value'] = 'assets/templates/default/css/';
        }

        if (!empty($properties['vars'][0])) {
            $properties['vars'][0]['value'] = 'assets/templates/default/css/variables.json';
        }

        $modx->db->update(['properties' => json_encode($properties, JSON_UNESCAPED_UNICODE)], $site_plugins, "`id` = '" . $plugin['id'] . "'");
    }
}

$site_modules = $modx->getFullTablename('site_modules');

$module = $modx->db->getRow($modx->db->select('*', $site_modules, "`name` = 'eLists'"));

if ($module) {
    $properties = json_decode($module['properties'], true);

    if ($properties) {
        if (!empty($properties['param_tv_id'][0])) {
            $properties['param_tv_id'][0]['value'] = $tvs['tovarparams'];
        }

        if (!empty($properties['product_templates_id'][0])) {
            $properties['product_templates_id'][0]['value'] = $templates['product'];
        }

        if (!empty($properties['param_cat_id'][0])) {
            $category = $modx->db->getValue($modx->db->select('id', $modx->getFullTablename('categories'), "`category` = 'Параметры'"));

            if (!empty($category)) {
                $properties['param_cat_id'][0]['value'] = $category;
            }
        }

        if (!empty($properties['tovarChunkName'][0])) {
            $properties['tovarChunkName'][0]['value'] = 'products_item';
        }

        $modx->db->update(['properties' => json_encode($properties, JSON_UNESCAPED_UNICODE)], $site_modules, "`id` = '" . $module['id'] . "'");
    }
}

$system_settings = $modx->getFullTablename('system_settings');

$settings = [
    'group_tvs'                   => 2,
    'global_tabs'                 => 1,
    'client_company_logo'         => '',
    'client_company_address'      => 'г.Пермь, ул.Уличная, д.37, офис 142',
    'client_company_phone'        => '8 800 123 4567',
    'client_company_email'        => 'mail@' . $_SERVER['HTTP_HOST'],
    'client_head_text'            => "Проектирование и строительство\r\nсовременных домов в Перми и крае",
    'client_consultation_text'    => 'Подробно расскажем о наших услугах, видах работ и типовых проектах, рассчитаем стоимость и подготовим индивидуальное предложение!',
    'client_images_alg'           => '0',
    'client_social_facebook'      => '#',
    'client_social_vkontakte'     => '#',
    'client_social_instagram'     => '#',
    'client_social_youtube'       => '#',
    'client_social_odnoklassniki' => '#',
    'client_email_recipients'     => 'mnoskov@trylike.ru',
];

foreach ($settings as $key => $value) {
    $modx->db->save([
        'setting_name'  => $key,
        'setting_value' => $value,
    ], $system_settings, "`setting_name` = '$key'");
}


// remove installer
$query = $modx->db->select('id', $site_plugins, "`name` = 'Install'");

if ($id = $modx->db->getValue($query)) {
   $modx->db->delete($site_plugins, "`id` = '$id'");
   $modx->db->delete($site_plugin_events, "`pluginid` = '$id'");
};
