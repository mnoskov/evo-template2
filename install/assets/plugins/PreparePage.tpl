//<?php
/**
 * Prepare Page
 *
 * Page preparation
 *
 * @category    plugin
 * @author      mnoskov
 * @internal    @events OnAfterLoadDocumentObject,OnManagerPageInit
 * @internal    @installset base
*/

$cachename = MODX_BASE_PATH . 'assets/cache/constants.tpl2.pageCache.php';

if (file_exists($cachename)) {
    $data = json_decode(file_get_contents($cachename), true);
} else {
    $data = [];

    $data['attributes_category_id'] = $modx->db->getValue($modx->db->query("SELECT id FROM " . $modx->getFullTableName('categories') . " WHERE category = 'Параметры';"));

    $query = $modx->db->query("SELECT id, templatename FROM " . $modx->getFullTableName('site_templates'));

    while ($row = $modx->db->getRow($query)) {
        $data[$row['templatename'] . '_template_id'] = $row['id'];
    }

    file_put_contents($cachename, json_encode($data));
}

$modx->templateConstants = $data;

switch ($modx->Event->name) {
    case 'OnAfterLoadDocumentObject': {
        foreach ($data as $key => $value) {
            $modx->setPlaceholder('my_' . $key, $value);
        }

        break;
    }
}
