//<?php
/**
 * getImageColor
 *
 * getImageColor
 *
 * @category    snippet
 * @internal    @overwrite true
*/

/**
 * Usage:
 *
 * Get image average color:
 * [[getImageColor? &source=`assets/images/example.jpg`]]
 *
 * Get image average color, return monotone 1x1 image:
 * [[getImageColor? &source=`assets/images/example.jpg` &output=`image`]]
 *
 * Get image average color, return monotone image with custom size:
 * [[getImageColor? &source=`assets/images/example.jpg` &output=`image` &width=`160` &height=`160`]]
 *
 * Get image contrast color hex representation, #000 or #fff:
 * [[getImageColor? &source=`assets/images/example.jpg` &contrast=`1`]]
 *
 * Get image contrast color hex representation, custom colors:
 * [[getImageColor? &source=`assets/images/example.jpg` &contrast=`1` &light=`#eff` &dark=`#333`]]
 *
 * Get image contrast color as image:
 * [[getImageColor? &source=`assets/images/example.jpg` &contrast=`1` &output=`image`]]
 *
 * Get contrast color hex representation:
 * [[getImageColor? &source=`#165837` &contrast=`1`]]
 */

if (empty($source)) {
    return '';
}

$source = trim($source);

if (is_readable(MODX_BASE_PATH . $source)) {
    // It's a file
    $source = MODX_BASE_PATH . $source;

    try {
        if (function_exists('exif_imagetype')) {
            $type = exif_imagetype($source);
        } else {
            $info = getimagesize($source);
            $type = $info[2];
        }
    } catch (\Exception $e) {
        return '';
    }

    if (!in_array($type, [IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP])) {
        return '';
    }

    try {
        switch ($type) {
            case IMAGETYPE_GIF:  $image = imagecreatefromgif($source);  break;
            case IMAGETYPE_JPEG: $image = imagecreatefromjpeg($source); break;
            case IMAGETYPE_PNG:  $image = imagecreatefrompng($source);  break;
            case IMAGETYPE_BMP:  $image = imagecreatefrombmp($source);  break;
        }

        $scaled = imagescale($image, 1, 1, IMG_BICUBIC_FIXED);
        $index  = imagecolorat($scaled, 0, 0);
        $rgb    = imagecolorsforindex($scaled, $index);
        imagedestroy($image);
        imagedestroy($scaled);
    } catch (\Exception $e) {
        return '';
    }

    $color = sprintf('#%02X%02X%02X', $rgb['red'], $rgb['green'], $rgb['blue']);
    $rgb = array_values($rgb);
} else {
    // It's a color
    if (preg_match('/^#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])$/', $source, $m)) {
        $color = '#' . $m[1] . $m[1] . $m[2] . $m[2] . $m[3] . $m[3];
        $rgb = [hexdec($m[1] . $m[1]), hexdec($m[2] . $m[2]), hexdec($m[3] . $m[3])];
    } else if (preg_match('/^rgba?\((\d+), *(\d+), *(\d+)/', $source, $m)) {
        $color = '#' . dechex($m[1]) . dechex($m[2]) . dechex($m[3]);
        $rgb = [$m[1], $m[2], $m[3]];
    } else {
        $color = $source;
        preg_match('/^#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})$/', $source, $m);
        $rgb = [hexdec($m[1]), hexdec($m[2]), hexdec($m[3])];
    }
}

if (!empty($contrast)) {
    $dark = isset($dark) ? $dark : '#000';
    $light = isset($light) ? $light : '#fff';
    $gray  = max(0, min(255, round(0.3 * $rgb[0] + 0.6 * $rgb[1] + 0.25 * $rgb[2])));
    $color = ($gray > 127) ? $dark : $light;
}

if (isset($output) && $output == 'image') {
    $width  = isset($width) ? $width : 1;
    $height = isset($height) ? $height : 1;

    if (!empty($contrast)) {
        if (strlen($color) == 4) {
            preg_match('/^#(.)(.)(.)$/', $color, $m);
            $rgb = [
                hexdec($m[1] . $m[1]),
                hexdec($m[2] . $m[2]),
                hexdec($m[3] . $m[3]),
            ];
        } else {
            preg_match('/^#(..)(..)(..)$/', $color, $m);
            $rgb = [
                hexdec($m[1]),
                hexdec($m[2]),
                hexdec($m[3]),
            ];
        }
    }

    $image = imagecreatetruecolor($width, $height);
    imagefill($image, 0, 0, imagecolorallocate($image, $rgb[0], $rgb[1], $rgb[2]));

    ob_start();
    imagepng($image);
    $bin = ob_get_clean();
    imagedestroy($image);

    return 'data:image/png;base64,' . base64_encode($bin);
}

return $color;
