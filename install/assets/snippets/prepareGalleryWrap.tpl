/**
 * prepareGalleryWrap
 * 
 * prepareGalleryWrap
 * 
 * @category    snippet
 * @internal    @overwrite true
*/
//<?php

$tv = $modx->getTemplateVarOutput(['gallery_format'], $data['parent']);
$data['placeholders']['format'] = empty($tv['gallery_format']) ? '-default' : $tv['gallery_format'];
return $data;
