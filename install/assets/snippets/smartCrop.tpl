/**
 * smartCrop
 *
 * smartCrop
 *
 * @category    snippet
 * @internal    @overwrite true
*/
//<?php
$length = isset($length) && is_numeric($length) && $length > 0 ? $length : 100;

if (empty($str)) {
    return '';
}

$str   = trim(strip_tags($str));
$left  = mb_substr($str, 0, $length);
$right = mb_substr($str, $length);

if ($left[strlen($left) - 1] != ' ') {
    $right = substr($right, 0, strpos($right, ' '));
    $left .= $right;
}

return rtrim($left, ' .,:-') . '...';
