/**
 * prepareMainServicesItem
 * 
 * prepareMainServicesItem
 * 
 * @category    snippet
 * @internal    @overwrite true
*/
//<?php

if (in_array($data['iteration'], [1, 6])) {
    $data['imagesize']   = 'w=498,h=234';
    $data['customclass'] = 'large';
} else {
    $data['imagesize'] = 'w=245,h=234';
    $data['customclass'] = '';
}

return $data;
