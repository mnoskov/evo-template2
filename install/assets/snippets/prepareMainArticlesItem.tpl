/**
 * prepareMainArticlesItem
 * 
 * prepareMainArticlesItem
 * 
 * @category    snippet
 * @internal    @overwrite true
*/
//<?php

if ($data['iteration'] == 1) {
    $data['imagesize']   = 'w=668,h=417';
    $data['customclass'] = 'large';
} else {
    $data['imagesize'] = 'w=322,h=201';
    $data['customclass'] = '';
}

return $data;
