//<?php
/**
 * renderAttributes
 *
 * renderAttributes
 *
 * @category    snippet
 * @internal    @overwrite true
*/

$docid    = isset($docid) ? $docid : $modx->documentIdentifier;
$tpl      = isset($tpl) ? $tpl : '@CODE:<tr><td>[+caption+]:&nbsp;</td><td>[+value+]</td></tr>';
$outerTpl = isset($outerTpl) ? $outerTpl : '@CODE:<table>[+wrap+]</table>';
$exclude  = isset($exclude) ? explode(',', $exclude) : [];
$include  = isset($include) ? explode(',', $include) : [];
$orderSet = isset($orderSet) ? $orderSet : null;

if (!isset($category) && empty($include)) {
    return 'Required parameter "category" is missing!';
}

require_once MODX_BASE_PATH . 'assets/snippets/DocLister/lib/DLTemplate.class.php';
$DLTemplate = DLTemplate::getInstance($modx);

$category = isset($category) ? $category : -1;

if ($docid == $modx->documentIdentifier) {
    $tplid = $modx->documentObject['template'];
} else {
    $tplid = $modx->runSnippet('DocInfo', ['docid' => $docid, 'field' => 'template']);
}

$query = $modx->db->query("
    SELECT tv.*, IFNULL(tvc.value, tv.default_text) AS value
    FROM " . $modx->getFullTableName('site_tmplvars') . " tv
    JOIN " . $modx->getFullTableName('site_tmplvar_templates') . " tt ON tt.templateid = '$tplid' AND tt.tmplvarid = tv.id
    LEFT JOIN " . $modx->getFullTableName('site_tmplvar_contentvalues') . " tvc ON tvc.tmplvarid = tv.id
    WHERE (tv.category = '$category' OR tv.name IN ('" . implode("', '", $include) . "'))
    AND tvc.contentid = '$docid'
    AND tv.name NOT IN ('" . implode("', '", $exclude) . "')
    ORDER BY " . (!empty($orderSet) ? "FIND_IN_SET(tv.name, '$orderSet')" : 'tt.rank') . ";
");

$attributes = [];

while ($row = $modx->db->getRow($query)) {
    if (!empty($row['elements'])) {
        if (!function_exists('ParseIntputOptions')) {
            require_once(MODX_MANAGER_PATH . 'includes/tmplvars.inc.php');
        }

        if (!function_exists('ProcessTVCommand')) {
            require_once(MODX_MANAGER_PATH . 'includes/tmplvars.commands.inc.php');
        }

        $elements = ParseIntputOptions(ProcessTVCommand($row['elements'], '', '', 'tvform', $tv = []));
        $values   = [];

        if (!empty($elements)) {
            foreach ($elements as $element) {
                list($val, $key) = is_array($element) ? $element : explode('==', $element);

                if (strlen($val) == 0) {
                    $val = $key;
                }

                if (strlen($key) == 0) {
                    $key = $val;
                }

                $values[$key] = $val;
            }

            if (isset($values[ $row['value'] ])) {
                $row['value'] = $values[ $row['value'] ];
            }
        }
    }

    $attributes[] = $row;
}

$out = '';

foreach ($attributes as $attr) {
    if (!empty($attr['value'])) {
        $out .= $DLTemplate->parseChunk($tpl, $attr);
    }
}

if (!empty($out)) {
    $out = $DLTemplate->parseChunk($outerTpl, ['wrap' => $out]);
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $out);
    $modx->setPlaceholder($toPlaceholder . '_notempty', !empty($out) ? 1 : 0);
    return '';
}

return $out;
