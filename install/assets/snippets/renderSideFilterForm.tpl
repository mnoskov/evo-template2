/**
 * renderSideFilterForm
 *
 * renderSideFilterForm
 *
 * @category    snippet
 * @internal    @overwrite true
*/
//<?php
$modx->runSnippet('eFilter', [
    'cfg'   => 'template2',
    'depth' => 0,
    'ajax'  => 1,
]);

$form = $modx->getPlaceholder('eFilter_form');

if (!empty($form)) {
    return $modx->parseChunk('fixed_filters_form', [
        'form' => $form,
    ], '[+', '+]');
}
