/**
 * renderProducts
 *
 * renderProducts
 *
 * @category    snippet
 * @internal    @overwrite true
*/
//<?php
$items = [12 => 12, 25 => 25, 50 => 50, 100 => 100];

if (isset($_GET['display']) && isset($items[$_GET['display']])) {
    $current = $_GET['display'];
    $_SESSION['display'] = $current;
} else if (isset($_SESSION['display']) && isset($items[$_SESSION['display']])) {
    $current = $_SESSION['display'];
}

if (empty($current)) {
    $current = 12;
    $_SESSION['display'] = $current;
}

$out = '';

foreach ($items as $item) {
    $out .= '<option value="' . $item . '"' . ($item == $current ? ' selected' : '') . '>' . $item . '</option>';
}

$displaySelector = '<select name="display" class="form-control" onchange="location = location.href.replace(/[&\?]display=\d+/, \'\') + \'&display=\' + this.value;">' . $out . '</select>';

$items = [
    0 => ['title' => 'По умолчанию', 'sortBy' => 'menuindex', 'sortOrder' => 'ASC'],
    1 => ['title' => 'Сначала дешевые', 'sortBy' => 'price', 'sortOrder' => 'ASC'],
    2 => ['title' => 'Сначала дорогие', 'sortBy' => 'price', 'sortOrder' => 'DESC'],
];

if (isset($_GET['sort']) && isset($items[$_GET['sort']])) {
    $key = $_GET['sort'];
    $_SESSION['sort'] = $key;
} else if (isset($_SESSION['sort']) && isset($items[$_SESSION['sort']])) {
    $key = $_SESSION['sort'];
}

if (empty($key)) {
    $current = reset($items);
    $key = key($items);
} else {
    $current = $items[$key];
}

$_SESSION['sortBy'] = $current['sortBy'];
$_SESSION['sortOrder'] = $current['sortOrder'];

$out = '';

foreach ($items as $i => $item) {
    $out .= '<option value="' . $i . '"' . ($i == $key ? ' selected' : '') . '>' . $item['title'] . '</option>';
}

$sortSelector =  '<select name="sort" class="form-control" onchange="location = location.href.replace(/[&\?]sort=\d+/, \'\') + \'&sort=\' + this.value;" style="display: inline-block; width: auto;">' . $out . '</select>';

$controls = '
    <div class="catalog-control">
        <div class="pull-xs-right">
            ' . $displaySelector . '
        </div>

        ' . $sortSelector . '
    </div>
';

$sortBy    = isset($_SESSION['sortBy']) ? $_SESSION['sortBy'] : 'menuindex';
$sortOrder = isset($_SESSION['sortOrder']) ? $_SESSION['sortOrder'] : 'ASC';
$display   = isset($_SESSION['display']) ? $_SESSION['display'] : 12;

$products = $modx->runSnippet('eFilterResult', [
    'parents'         => $modx->documentIdentifier,
    'depth'           => 0,
    'tvList'          => 'image,price,old_price',
    'sortBy'          => $sortBy,
    'sortOrder'       => $sortOrder,
    'display'         => $display,
    'paginate'        => 'pages',
    'noneWrapOuter'   => 0,
    'tpl'             => 'products_item',
    'ownerTPL'        => 'products_wrap',
    'TplCurrentPage'  => '@CODE:<span class="page current">[+num+]</span>',
    'TplWrapPaginate' => '@CODE:<div class="pagination">[+wrap+]</div>',
    'TplPrevP'        => '@CODE: ',
    'TplNextP'        => '@CODE: ',
]);

if (!empty($products)) {
    return $controls . $products;
}

if (isset($_GET['f'])) {
    return '<div id="eFiltr_results">Ничего не найдено</div>';
}
