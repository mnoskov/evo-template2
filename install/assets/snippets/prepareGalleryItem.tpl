/**
 * prepareGalleryItem
 * 
 * prepareGalleryItem
 * 
 * @category    snippet
 * @internal    @overwrite true
*/
//<?php

$format = $_extDocLister->getStore('format');

if (is_null($format)) {
    $tv = $modx->getTemplateVarOutput(['gallery_format'], $data['parent']);
    $format = $tv['gallery_format'];
    $_extDocLister->setStore('format', $format);
}

switch ($format) {
    case 1:  $data['dimensions'] = 'w=260,h=324,far=C,bg=FFFFFF,f=jpg'; break;
    default: $data['dimensions'] = 'w=263,h=170,zc=1,f=jpg';
}

return $data;
