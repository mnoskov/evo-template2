/**
 * prepareCategories
 * 
 * prepareCategories
 * 
 * @category    snippet
 * @internal    @overwrite true
*/
//<?php
if ($_DocLister->getCFGDef('tplCode') == 'categories_tpl3') {
    $data['children'] = $modx->runSnippet('DocLister', [
        'parents'      => $data['id'],
        'tpl'          => '@CODE: <a href="[+url+]">[+pagetitle+]</a>',
        'ownerTPL'     => '@CODE: <div class="children">[+dl.wrap+]</div>',
        'addWhereList' => "c.template = '" . $_DocLister->getCFGDef('filterTpl') . "'",
        'orderBy'      => 'c.menuindex ASC',
    ]);
}

return $data;
