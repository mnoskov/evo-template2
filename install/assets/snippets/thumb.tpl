/**
 * thumb
 *
 * thumb
 *
 * @category    snippet
 * @internal    @overwrite true
*/

$options = strtr($options, ["," => "&", "_" => "=", '{' => '[', '}' => ']']);
parse_str($options, $options);

$resizeMethod = isset($resizeMethod) ? $resizeMethod : $modx->getConfig('client_images_alg');

switch ($resizeMethod) {
	case 0: {
		$options['zc'] = 1;
		if (isset($options['far'])) {
			unset($options['far']);
		}
		break;
	}

	case 1: {
		$options['far'] = 'C';
		$options['bg']  = 'FFFFFF';
		if (isset($options['zc'])) {
			unset($options['zc']);
		}
		break;
	}
}
$params['options'] = http_build_query($options);
$params['adBlockFix'] = 1;
return $modx->runSnippet('phpthumb', $params);
