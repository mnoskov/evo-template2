﻿/**
 * head_background
 *
 * Фон шапки
 *
 * @category        tv
 * @name            head_background
 * @internal        @caption Фон шапки
 * @internal        @input_type image
 * @internal        @template_assignments *
 * @internal        @modx_category Шапка
 */