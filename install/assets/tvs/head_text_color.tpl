﻿/**
 * head_text_color
 *
 * Цвет текста
 *
 * @category        tv
 * @name            head_text_color
 * @internal        @caption Цвет текста
 * @internal        @input_type text
 * @internal        @template_assignments *
 * @internal        @modx_category Шапка
 */