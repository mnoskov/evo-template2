﻿/**
 * price
 *
 * Цена
 *
 * @category        tv
 * @name            price
 * @internal        @caption Цена, руб.
 * @internal        @input_type number
 * @internal        @input_options 
 * @internal        @input_default 
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @modx_category Параметры
 * @internal        @template_assignments product
 */