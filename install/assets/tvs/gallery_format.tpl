﻿/**
 * gallery_format
 *
 * Формат галереи
 *
 * @category        tv
 * @name            gallery_format
 * @internal        @caption Формат галереи
 * @internal        @input_type option
 * @internal        @input_options 4x3, по 4 в строке==0||3x4, по 3 в строке==1
 * @internal        @input_default 0
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 * @internal        @template_assignments gallery
 */