﻿/**
 * head_image
 *
 * Изображение в шапке
 *
 * @category        tv
 * @name            head_image
 * @internal        @caption Изображение в шапке
 * @internal        @input_type image
 * @internal        @template_assignments *
 * @internal        @modx_category Шапка
 */