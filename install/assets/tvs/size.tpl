﻿/**
 * size
 *
 * Диагональ телевизора, дюймов
 *
 * @category        tv
 * @name            size
 * @internal        @caption Диагональ телевизора, дюймов
 * @internal        @input_type text
 * @internal        @template_assignments product
 * @internal        @modx_category Параметры
 */