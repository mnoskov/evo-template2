﻿/**
 * dimension
 *
 * Разрешение экрана
 *
 * @category        tv
 * @name            dimension
 * @internal        @caption Разрешение экрана
 * @internal        @input_type text
 * @internal        @template_assignments product
 * @internal        @modx_category Параметры
 */