﻿/**
 * head_type
 *
 * Тип шапки
 *
 * @category        tv
 * @name            head_type
 * @internal        @caption Тип шапки
 * @internal        @input_type option
 * @internal        @input_options Простая==_default||Расширенная==_extended
 * @internal        @input_default _default
 * @internal        @template_assignments *
 * @internal        @modx_category Шапка
 */